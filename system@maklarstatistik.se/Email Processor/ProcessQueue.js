/**
 * @author Mats Blomdahl <mats.blomdahl@gmail.com>
 * @name ProcessQueue.gs
 * @version 0.3
 *
 * @description
 * Processes for interacting with labels used by the `Email Processor` Apps Scripts project
 */

/**
 * # Reads threads range from the target label and calls `forEachCallback(thread)`
 *
 * @param {GmailLabel} label
 * @param {Number} maxThreads
 * @param {Function} forEachCallback
 * @param {Boolean} readDescending
 * @private
 */
function _getLabelThreads(label, maxThreads, forEachCallback, readDescending) {
    readDescending = readDescending || false;

    // Max label threads available
    var unreadCount = label.getUnreadCount();

    // Label threads read offset (start from the bottom)
    var threadsReadOffset = 0;
    if (!readDescending) {
        threadsReadOffset = Math.max(unreadCount - maxThreads, 0);
    }

    label.getThreads(threadsReadOffset, maxThreads).forEach(forEachCallback);
}

/**
 * # ``Stage-1 Process``
 *
 * Process responsible for deleting duplicate messages, initial ScriptDb ingestion and transport to `preProcessed`
 * label. The process does essentially 3 things:
 *
 * 1.  Deletes duplicate messages (and threads) based on origin message metadata and adds ScriptDb deletion records
 *
 * 2.  Performs initial ScriptDb ingestion for unique messages/threads
 *
 * 3.  Adds `preProcessed` label and removes the `processingQueue` label for the import path
 *
 * @param {Number|} maxThreads
 * @returns {{messages: Number, threads: Number}} Threads and messages added to ScriptDb.
 */
function processQueueToPreProcessed(maxThreads) {
    maxThreads = maxThreads || 12;
    var threadMsgsToScriptDb = 0;
    var threadsToScriptDb = 0;
    var labelCategory;

    for (labelCategory in labels) {
        // Origin email address for GAE ingestion
        var gaeIngestionFromAddr = labels[labelCategory].parent.getName();

        // The `processingQueue` label; where all inbound messages arrive by default
        var processingQueue = labels[labelCategory].queue;
        var processingQueueName = processingQueue.getName();

        // The `preProcessed` label; where messages are stored after successful check for duplicate msgs
        var preProcessedQueue = labels[labelCategory].preProcessed;
        var preProcessedQueueName = preProcessedQueue.getName();

        Utilities.sleep(5e2);

        _getLabelThreads(processingQueue, maxThreads, function(thread) {
            var messageCount = thread.getMessageCount();
            var threadId = thread.getId();

            // Used for debug / performance audit
            var duplicateMsgsRemoved = 0;

            // Message configuration objects
            var messageCfgs = [];

            // Assigned using the message's origin metadata
            var originThreadId;

            // Assert Gmail `conversation view off` config is working => IT'S NOT
            if (messageCount !== 1) {
                //throw Error('msgCnt for thread id ' + thread.getId() + ': ' + messageCount + ' !== 1');
                Logger.log('msgCnt for thread id ' + thread.getId() + ': ' + messageCount + ' !== 1');
            } else {
                Utilities.sleep(5e2);
            }

            // Assert thread isn't already recorded in ScriptDb
            if (getScriptDbThreads({ thread_id: threadId }, { limit: 1 }).length) {
                throw Error('ScriptDb records for thread ' + threadId + ' already recorded');
            }

            // Process thread messages
            thread.getMessages().forEach(function(message) {
                Utilities.sleep(5e2);

                if (message.isInTrash()) {
                    duplicateMsgsRemoved++;
                    return;
                } else {
                    Utilities.sleep(5e2);
                }

                // Extract origin metadata from attachment
                var attachments = message.getAttachments();
                var originMeta = getAttachmentsOriginMetaFrom(attachments);
                originThreadId = originMeta.thread_id;

                // Get from ScriptDb by origin metadata
                var scriptDbMsgs = getScriptDbMessages({
                    origin_message_id: originMeta.message_id,
                    origin_date: originMeta.date
                }, { limit: 1 });

                if (scriptDbMsgs.length) {
                    // Delete any duplicates
                    Logger.log('Duplicate msg found (message_id ' + message.getId() + ')');
                    Utilities.sleep(5e2);

                    message.moveToTrash();
                    duplicateMsgsRemoved++;

                    // Add ScriptDb record of trashed message
                    var cfg = addScriptDbRmMessage({
                        origin_date: originMeta.date,
                        thread_id: threadId,
                        origin_thread_id: originThreadId,
                        message_id: message.getId(),
                        origin_message_id: originMeta.message_id,
                        forward_from: gaeIngestionFromAddr,
                        remove_label: processingQueueName,
                        sys_dirty: false
                    });

                    if (!cfg) {
                        throw Error('ScriptDb write of trashing-notice for message_id ' + message.getId() + ' failed');
                    } else {
                        messageCfgs.push(cfg);
                    }

                } else {
                    // Add ScriptDb record of message
                    Logger.log('Unique msg found (message_id ' + message.getId() + ')');
                    Utilities.sleep(5e2);

                    var cfg = addScriptDbMessage({
                        origin_date: originMeta.date,
                        thread_id: threadId,
                        origin_thread_id: originThreadId,
                        message_id: message.getId(),
                        origin_message_id: originMeta.message_id,
                        forward_from: gaeIngestionFromAddr,
                        add_label: preProcessedQueueName,
                        remove_label: processingQueueName,
                        sys_dirty: true
                    });

                    if (!cfg) {
                        throw Error('ScriptDb write for unique message_id ' + message.getId() + ' failed');
                    } else {
                        messageCfgs.push(cfg);
                        threadMsgsToScriptDb++;
                    }
                }

                Utilities.sleep(5e2);
            });

            // Parent thread post-processing
            if (duplicateMsgsRemoved == messageCount) {
                // Delete parent thread if depleted of messages
                Utilities.sleep(5e2);
                thread.moveToTrash();

                // Remove from `processing queue` label
                Utilities.sleep(1e3);
                thread.removeLabel(processingQueue);

                // Add ScriptDb record for the trashed thread
                var cfg = addScriptDbRmThread({
                    thread_id: threadId,
                    origin_thread_id: originThreadId,
                    message_count: messageCount,
                    remove_label: processingQueueName,
                    forward_from: gaeIngestionFromAddr,
                    sys_dirty: false
                });

                cfg.messageCfgs = messageCfgs;

                /* DEBUG */
                Logger.log(Utilities.jsonStringify(cfg));
                /* DEBUG */

                /* DEBUG */
                Logger.log('toScriptDb(msg): ' + threadMsgsToScriptDb + ' / toScriptDb(thr): ' + threadsToScriptDb);
                /* DEBUG */

            } else {

                if (duplicateMsgsRemoved) {
                    // Apply labels transition on thread
                    Utilities.sleep(5e2);

                    // Refresh thread to sync message deletions
                    thread = thread.refresh();
                }

                Utilities.sleep(5e2);
                thread.addLabel(preProcessedQueue);

                // Add ScriptDb record of thread
                var cfg = addScriptDbThread({
                    thread_id: threadId,
                    origin_thread_id: originThreadId,
                    message_count: thread.getMessageCount(),
                    remove_label: processingQueueName,
                    add_label: preProcessedQueueName,
                    forward_from: gaeIngestionFromAddr,
                    sys_dirty: true
                });

                if (!cfg) {
                    throw Error('Writing thread id ' + threadId + ' to ScriptDb failed');
                } else {
                    threadsToScriptDb++;
                }

                // Label removal should never succeed unless registration in ScriptDb succeeds
                Utilities.sleep(5e2);
                thread.removeLabel(processingQueue);

                cfg.messageCfgs = messageCfgs;

                /* DEBUG */
                Logger.log(Utilities.jsonStringify(cfg));
                /* DEBUG */

                /* DEBUG */
                Logger.log('toScriptDb(msg): ' + threadMsgsToScriptDb + ' / toScriptDb(thr): ' + threadsToScriptDb);
                /* DEBUG */
            }

        });
    }

    return {
        messages: threadMsgsToScriptDb,
        threads: threadsToScriptDb
    };
}

/**
 * # ``Stage-2 Process``
 *
 * Reads messages (i.e. threads) from the `preProcessed` label and performs clean-up of `processingQueue` labels. The
 * entries are then added to the `processed` label and have their `preProcessed` label removed.
 *
 * @param {Number|} maxThreads
 * @returns {{messages: Number, threads: Number}} Threads and messages added to ScriptDb.
 */
function processPreProcessedToProcessed(maxThreads) {
    maxThreads = maxThreads || 12;
    var threadsProcessed = 0;
    var messagesProcessed = 0;
    var labelCategory;

    for (labelCategory in labels) {
        // The `preProcessed` label; where messages are stored after successful check for duplicate msgs
        var preProcessedQueue = labels[labelCategory].preProcessed;
        var preProcessedQueueName = preProcessedQueue.getName();

        // The `processed` label; where messages are stored prior to forwarding
        var processingQueue = labels[labelCategory].queue;
        var processingQueueName = processingQueue.getName();

        Utilities.sleep(5e2);

        _getLabelThreads(preProcessedQueue, maxThreads, function(thread) {
            var messageCount = thread.getMessageCount();
            var threadId = thread.getId();

            // Assigned using the message's origin metadata
            var originThreadId;

            // Assert Gmail `conversation view off` config is working => IT'S NOT
            if (messageCount !== 1) {
                //throw Error('msgCnt for thread id ' + thread.getId() + ': ' + messageCount + ' !== 1');
                Logger.log('msgCnt for thread id ' + thread.getId() + ': ' + messageCount + ' !== 1');
            } else {
                Utilities.sleep(5e2);
            }

            // Assert thread isn't already recorded in ScriptDb
            if (getScriptDbThreads({ thread_id: threadId }, { limit: 1 }).length) {
                throw Error('ScriptDb records for thread ' + threadId + ' already recorded');
            }

            // Process thread messages
            thread.getMessages().forEach(function(message) {
                Utilities.sleep(5e2);

                if (message.isInTrash()) {
                    duplicateMsgsRemoved++;
                    return;
                } else {
                    Utilities.sleep(5e2);
                }

                // Extract origin metadata from attachment
                var attachments = message.getAttachments();
                var originMeta = getAttachmentsOriginMetaFrom(attachments);
                originThreadId = originMeta.thread_id;

                // Get from ScriptDb by origin metadata
                var scriptDbMsgs = getScriptDbMessages({
                    origin_message_id: originMeta.message_id,
                    origin_date: originMeta.date
                }, { limit: 1 });

                if (scriptDbMsgs.length) {
                    // Delete any duplicates
                    Logger.log('Duplicate msg found (message_id ' + message.getId() + ')');
                    Utilities.sleep(5e2);

                    message.moveToTrash();
                    duplicateMsgsRemoved++;

                    // Add ScriptDb record of trashed message
                    var cfg = addScriptDbRmMessage({
                        origin_date: originMeta.date,
                        thread_id: threadId,
                        origin_thread_id: originThreadId,
                        message_id: message.getId(),
                        origin_message_id: originMeta.message_id,
                        forward_from: gaeIngestionFromAddr,
                        remove_label: processingQueueName,
                        sys_dirty: false
                    });

                    if (!cfg) {
                        throw Error('ScriptDb write of trashing-notice for message_id ' + message.getId() + ' failed');
                    } else {
                        messageCfgs.push(cfg);
                    }

                } else {
                    // Add ScriptDb record of message
                    Logger.log('Unique msg found (message_id ' + message.getId() + ')');
                    Utilities.sleep(5e2);

                    var cfg = addScriptDbMessage({
                        origin_date: originMeta.date,
                        thread_id: threadId,
                        origin_thread_id: originThreadId,
                        message_id: message.getId(),
                        origin_message_id: originMeta.message_id,
                        forward_from: gaeIngestionFromAddr,
                        add_label: preProcessedQueueName,
                        remove_label: processingQueueName,
                        sys_dirty: true
                    });

                    if (!cfg) {
                        throw Error('ScriptDb write for unique message_id ' + message.getId() + ' failed');
                    } else {
                        messageCfgs.push(cfg);
                        threadMsgsToScriptDb++;
                    }
                }

                Utilities.sleep(5e2);
            });

            // Parent thread post-processing
            if (duplicateMsgsRemoved == messageCount) {
                // Delete parent thread if depleted of messages
                Utilities.sleep(5e2);
                thread.moveToTrash();

                // Remove from `processing queue` label
                Utilities.sleep(1e3);
                thread.removeLabel(processingQueue);

                // Add ScriptDb record for the trashed thread
                var cfg = addScriptDbRmThread({
                    thread_id: threadId,
                    origin_thread_id: originThreadId,
                    message_count: messageCount,
                    remove_label: processingQueueName,
                    forward_from: gaeIngestionFromAddr,
                    sys_dirty: false
                });

                cfg.messageCfgs = messageCfgs;

                /* DEBUG */
                Logger.log(Utilities.jsonStringify(cfg));
                /* DEBUG */

                /* DEBUG */
                Logger.log('toScriptDb(msg): ' + threadMsgsToScriptDb + ' / toScriptDb(thr): ' + threadsToScriptDb);
                /* DEBUG */

            } else {

                if (duplicateMsgsRemoved) {
                    // Apply labels transition on thread
                    Utilities.sleep(5e2);

                    // Refresh thread to sync message deletions
                    thread = thread.refresh();
                }

                Utilities.sleep(5e2);
                thread.addLabel(preProcessedQueue);

                // Add ScriptDb record of thread
                var cfg = addScriptDbThread({
                    thread_id: threadId,
                    origin_thread_id: originThreadId,
                    message_count: thread.getMessageCount(),
                    remove_label: processingQueueName,
                    add_label: preProcessedQueueName,
                    forward_from: gaeIngestionFromAddr,
                    sys_dirty: true
                });

                if (!cfg) {
                    throw Error('Writing thread id ' + threadId + ' to ScriptDb failed');
                } else {
                    threadsToScriptDb++;
                }

                // Label removal should never succeed unless registration in ScriptDb succeeds
                Utilities.sleep(5e2);
                thread.removeLabel(processingQueue);

                cfg.messageCfgs = messageCfgs;

                /* DEBUG */
                Logger.log(Utilities.jsonStringify(cfg));
                /* DEBUG */

                /* DEBUG */
                Logger.log('toScriptDb(msg): ' + threadMsgsToScriptDb + ' / toScriptDb(thr): ' + threadsToScriptDb);
                /* DEBUG */
            }

        });
    }

    return {
        messages: threadMsgsToScriptDb,
        threads: threadsToScriptDb
    };
}

/**
 * # ``Stage-3 Process`` (SYNC)
 *
 * Reads messages (i.e. threads) from the `processed` label and performs clean-up of any remaining `preProcessed`
 * labels. The messages are then forwarded to their respective GAE ingestion address.
 *
 * Upon successful forwarding, each message is assigned to the `forwarded` queue and have their `processed` label
 * removed.
 *
 * @param {Number|} maxThreads
 * @returns {{messages: Number, threads: Number}} Threads and messages added to ScriptDb.
 */
function processQueueToPreProcessed(maxThreads) {
    maxThreads = maxThreads || 12;
    var threadMsgsToScriptDb = 0;
    var threadsToScriptDb = 0;
    var labelCategory;

    for (labelCategory in labels) {
        // Origin email address for GAE ingestion
        var gaeIngestionFromAddr = labels[labelCategory].parent.getName();

        // The `processingQueue` label; where all inbound messages arrive by default
        var processingQueue = labels[labelCategory].queue;
        var processingQueueName = processingQueue.getName();

        // The `preProcessed` label; where messages are stored after successful check for duplicate msgs
        var preProcessedQueue = labels[labelCategory].preProcessed;
        var preProcessedQueueName = preProcessedQueue.getName();

        Utilities.sleep(5e2);

        _getLabelThreads(processingQueue, maxThreads, function(thread) {
            var messageCount = thread.getMessageCount();
            var threadId = thread.getId();

            // Used for debug / performance audit
            var duplicateMsgsRemoved = 0;

            // Message configuration objects
            var messageCfgs = [];

            // Assigned using the message's origin metadata
            var originThreadId;

            // Assert Gmail `conversation view off` config is working => IT'S NOT
            if (messageCount !== 1) {
                //throw Error('msgCnt for thread id ' + thread.getId() + ': ' + messageCount + ' !== 1');
                Logger.log('msgCnt for thread id ' + thread.getId() + ': ' + messageCount + ' !== 1');
            } else {
                Utilities.sleep(5e2);
            }

            // Assert thread isn't already recorded in ScriptDb
            if (getScriptDbThreads({ thread_id: threadId }, { limit: 1 }).length) {
                throw Error('ScriptDb records for thread ' + threadId + ' already recorded');
            }

            // Process thread messages
            thread.getMessages().forEach(function(message) {
                Utilities.sleep(5e2);

                if (message.isInTrash()) {
                    duplicateMsgsRemoved++;
                    return;
                } else {
                    Utilities.sleep(5e2);
                }

                // Extract origin metadata from attachment
                var attachments = message.getAttachments();
                var originMeta = getAttachmentsOriginMetaFrom(attachments);
                originThreadId = originMeta.thread_id;

                // Get from ScriptDb by origin metadata
                var scriptDbMsgs = getScriptDbMessages({
                    origin_message_id: originMeta.message_id,
                    origin_date: originMeta.date
                }, { limit: 1 });

                if (scriptDbMsgs.length) {
                    // Delete any duplicates
                    Logger.log('Duplicate msg found (message_id ' + message.getId() + ')');
                    Utilities.sleep(5e2);

                    message.moveToTrash();
                    duplicateMsgsRemoved++;

                    // Add ScriptDb record of trashed message
                    var cfg = addScriptDbRmMessage({
                        origin_date: originMeta.date,
                        thread_id: threadId,
                        origin_thread_id: originThreadId,
                        message_id: message.getId(),
                        origin_message_id: originMeta.message_id,
                        forward_from: gaeIngestionFromAddr,
                        remove_label: processingQueueName,
                        sys_dirty: false
                    });

                    if (!cfg) {
                        throw Error('ScriptDb write of trashing-notice for message_id ' + message.getId() + ' failed');
                    } else {
                        messageCfgs.push(cfg);
                    }

                } else {
                    // Add ScriptDb record of message
                    Logger.log('Unique msg found (message_id ' + message.getId() + ')');
                    Utilities.sleep(5e2);

                    var cfg = addScriptDbMessage({
                        origin_date: originMeta.date,
                        thread_id: threadId,
                        origin_thread_id: originThreadId,
                        message_id: message.getId(),
                        origin_message_id: originMeta.message_id,
                        forward_from: gaeIngestionFromAddr,
                        add_label: preProcessedQueueName,
                        remove_label: processingQueueName,
                        sys_dirty: true
                    });

                    if (!cfg) {
                        throw Error('ScriptDb write for unique message_id ' + message.getId() + ' failed');
                    } else {
                        messageCfgs.push(cfg);
                        threadMsgsToScriptDb++;
                    }
                }

                Utilities.sleep(5e2);
            });

            // Parent thread post-processing
            if (duplicateMsgsRemoved == messageCount) {
                // Delete parent thread if depleted of messages
                Utilities.sleep(5e2);
                thread.moveToTrash();

                // Remove from `processing queue` label
                Utilities.sleep(1e3);
                thread.removeLabel(processingQueue);

                // Add ScriptDb record for the trashed thread
                var cfg = addScriptDbRmThread({
                    thread_id: threadId,
                    origin_thread_id: originThreadId,
                    message_count: messageCount,
                    remove_label: processingQueueName,
                    forward_from: gaeIngestionFromAddr,
                    sys_dirty: false
                });

                cfg.messageCfgs = messageCfgs;

                /* DEBUG */
                Logger.log(Utilities.jsonStringify(cfg));
                /* DEBUG */

                /* DEBUG */
                Logger.log('toScriptDb(msg): ' + threadMsgsToScriptDb + ' / toScriptDb(thr): ' + threadsToScriptDb);
                /* DEBUG */

            } else {

                if (duplicateMsgsRemoved) {
                    // Apply labels transition on thread
                    Utilities.sleep(5e2);

                    // Refresh thread to sync message deletions
                    thread = thread.refresh();
                }

                Utilities.sleep(5e2);
                thread.addLabel(preProcessedQueue);

                // Add ScriptDb record of thread
                var cfg = addScriptDbThread({
                    thread_id: threadId,
                    origin_thread_id: originThreadId,
                    message_count: thread.getMessageCount(),
                    remove_label: processingQueueName,
                    add_label: preProcessedQueueName,
                    forward_from: gaeIngestionFromAddr,
                    sys_dirty: true
                });

                if (!cfg) {
                    throw Error('Writing thread id ' + threadId + ' to ScriptDb failed');
                } else {
                    threadsToScriptDb++;
                }

                // Label removal should never succeed unless registration in ScriptDb succeeds
                Utilities.sleep(5e2);
                thread.removeLabel(processingQueue);

                cfg.messageCfgs = messageCfgs;

                /* DEBUG */
                Logger.log(Utilities.jsonStringify(cfg));
                /* DEBUG */

                /* DEBUG */
                Logger.log('toScriptDb(msg): ' + threadMsgsToScriptDb + ' / toScriptDb(thr): ' + threadsToScriptDb);
                /* DEBUG */
            }

        });
    }

    return {
        messages: threadMsgsToScriptDb,
        threads: threadsToScriptDb
    };
}
