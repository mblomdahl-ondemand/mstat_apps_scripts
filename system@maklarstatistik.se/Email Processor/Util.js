/**
 * @author Mats Blomdahl <mats.blomdahl@gmail.com>
 * @name Util.gs
 * @version 0.3
 *
 * @description
 * Utilities for the `Email Processor` Apps Scripts project
 */

/**
 * # Returns a labels mapping to pass around
 *
 * ## Structure
 *
 *     {
 *         capitex: {
 *             queue: GmailLabel,
 *             preProcessed: GmailLabel,
 *             processed: GmailLabel,
 *             forwarded: GmailLabel,
 *             parent: GmailLabel
 *         },
 *         sf: {
 *             queue: GmailLabel,
 *             preProcessed: GmailLabel,
 *             processed: GmailLabel,
 *             forwarded: GmailLabel,
 *             parent: GmailLabel
 *         },
 *         direct: {
 *             queue: GmailLabel,
 *             preProcessed: GmailLabel,
 *             processed: GmailLabel,
 *             forwarded: GmailLabel,
 *             parent: GmailLabel
 *         }
 *     }
 *
 * ## High-level workflow, step-by-step:
 *
 * 1.  Messages are sorted into different `queue` labels by the Gmail inbox filters.
 * 2.  A process deletes any duplicate messages based on origin message metadata. Unique messages are added to the
 *     `preProcessed` label.
 * 3.  A process reads messages (i.e. threads) from the `preProcessed` label and removes any remaining `queue` labels.
 *     The message (i.e. thread) is then added to the `processed` label.
 * 4.  A process reads messages (i.e. threads) from the `processed` label and forwards the message to GAE ingestion. The
 *     message (i.e. thread) is then added to the `forwarded` label.
 * 5.  A process reads messages (i.e. threads) from the `forwarded` label and clears any remaining `processed` labels.
 *
 * @returns {Object} Mapping of labels to different processing stages
 */
function getLabels() {
    var capitexParent = GmailApp.getUserLabelByName("capitex-import@maklarstatistik.se");
    var capitexProcessingQueue = GmailApp.getUserLabelByName("capitex-import@maklarstatistik.se/capitex-processing-queue");
    var capitexPreProcessed = GmailApp.getUserLabelByName("capitex-import@maklarstatistik.se/capitex-pre-processed");
    var capitexProcessed = GmailApp.getUserLabelByName("capitex-import@maklarstatistik.se/capitex-processed");
    var capitexForwarded = GmailApp.getUserLabelByName("capitex-import@maklarstatistik.se/capitex-forwarded");

    var sfParent = GmailApp.getUserLabelByName("sf-import@maklarstatistik.se");
    var sfProcessingQueue = GmailApp.getUserLabelByName("sf-import@maklarstatistik.se/sf-processing-queue");
    var sfPreProcessed = GmailApp.getUserLabelByName("sf-import@maklarstatistik.se/sf-pre-processed");
    var sfProcessed = GmailApp.getUserLabelByName("sf-import@maklarstatistik.se/sf-processed");
    var sfForwarded = GmailApp.getUserLabelByName("sf-import@maklarstatistik.se/sf-forwarded");

    var directParent = GmailApp.getUserLabelByName("direct-import@maklarstatistik.se");
    var directProcessingQueue = GmailApp.getUserLabelByName("direct-import@maklarstatistik.se/direct-processing-queue");
    var directPreProcessed = GmailApp.getUserLabelByName("direct-import@maklarstatistik.se/direct-pre-processed");
    var directProcessed = GmailApp.getUserLabelByName("direct-import@maklarstatistik.se/direct-processed");
    var directForwarded = GmailApp.getUserLabelByName("direct-import@maklarstatistik.se/direct-forwarded");

    return {
        capitex: {
            queue: capitexProcessingQueue,
            preProcessed: capitexPreProcessed,
            processed: capitexProcessed,
            forwarded: capitexForwarded,
            parent: capitexParent
        },
        sf: {
            queue: sfProcessingQueue,
            preProcessed: sfPreProcessed,
            processed: sfProcessed,
            forwarded: sfForwarded,
            parent: sfParent
        },
        direct: {
            queue: directProcessingQueue,
            preProcessed: directPreProcessed,
            processed: directProcessed,
            forwarded: directForwarded,
            parent: directParent
        }
    };
}








