/**
 * File: /mstat_apps_scripts/system@maklarstatistik.se/Email Processor/SourceMsg.js
 * Author: Mats Blomdahl
 * Version: 0.2
 */


function sourceThreads(labels, forwardsLim) {
    var labelsUnreadCount = [];

    for (var label in labels) {
        var unreadCount = labels[label].queue.getUnreadCount();
        Utilities.sleep(1000);
        if (unreadCount) {
            labelsUnreadCount.push([label, unreadCount]);

            /* DEBUG */
            Logger.log('found ' + unreadCount + ' unread for label ' + label);
            /* DEBUG */
        }
    }

    // Attempt even distribution across import paths
    var i;
    var threadsPerLabel = parseInt(forwardsLim / labelsUnreadCount.length);
    var rest = forwardsLim % labelsUnreadCount.length;

    for (i = labelsUnreadCount.length; i--;) {
        var unreadCount = labelsUnreadCount[i][1];
        var readCount = threadsPerLabel;
        if (rest && (readCount + rest) <= unreadCount) {
            readCount += rest;
            rest = 0;
        } else if (readCount > unreadCount) {
            readCount = unreadCount;
            rest += unreadCount - threadsPerLabel;
        }
        labelsUnreadCount[i].push(readCount);
    }

    var totalReadCount = 0;

    for (i = labelsUnreadCount.length; rest && i--;) {
        var unreadCount = labelsUnreadCount[i][1];
        var readCount = labelsUnreadCount[i][2];
        if ((readCount + rest) <= unreadCount) {
            labelsUnreadCount[i][2] += rest;
            rest = 0;
        } else {
            var countDx = unreadCount - readCount;
            labelsUnreadCount[i][2] += countDx;
            rest -= countDx;
        }
        totalReadCount += labelsUnreadCount[i][2];
    }

    var tpl = 'Label: %s / Unread: %i / ToRead: %i';
    for (i = labelsUnreadCount.length; i--;) {
        Logger.log(Utilities.formatString(tpl, labelsUnreadCount[i][0], labelsUnreadCount[i][1], labelsUnreadCount[i][2]));
    }

    if (totalReadCount > forwardsLim) {
        throw Error('Alg. error totalRC ' + totalReadCount + ' > maxTh ' + forwardsLim);
    } else {

        /* DEBUG */
        Logger.log(toLogLine('totalRC ' + totalReadCount + ' <= maxTh ' + forwardsLim));
        /* DEBUG */
    }

    // Load and submit threads to ´ScriptDb´ queue
    var threadMessagesToScriptDb = 0;
    for (i = labelsUnreadCount.length; i--;) {
        var threadReadCount = labelsUnreadCount[i][2];
        if (!threadReadCount) {
            continue;
        }
        var threadOffset = labelsUnreadCount[i][1] - threadReadCount;
        var fromLabel = labels[labelsUnreadCount[i][0]].queue;
        var fromLabelName = fromLabel.getName();
        var toLabelName = labels[labelsUnreadCount[i][0]].forwarded.getName();
        var forwardTo = labels[labelsUnreadCount[i][0]].parent.getName();
        fromLabel.getThreads(threadOffset, threadReadCount).forEach(function(thread) {
            var messageCount = thread.getMessageCount();
            var threadId = thread.getId();
            var messageCfgs = [];

            thread.getMessages().forEach(function(message) {

                var cfg = addScriptDbMessage({
                    thread_id: threadId,
                    message_id: message.getId(),
                    forward_to: forwardTo,
                    sys_dirty: true
                });

                if (cfg) {
                    messageCfgs.push(cfg);
                    threadMessagesToScriptDb++;
                }

            });

            var cfg = {
                thread_id: threadId,
                message_count: messageCount,
                remove_label: fromLabelName,
                add_label: toLabelName,
                forward_to: forwardTo,
                sys_dirty: true
            };

            var threadCfg = addScriptDbThread(cfg);

            if (messageCfgs.length && !threadCfg) { // threadCfg dirty
                threadCfg = replaceScriptDbThread(cfg);
                if (!threadCfg) {
                    throw Error('thread replace failure for id ' + threadId);
                }
            }

            if (threadCfg) {
                threadCfg.messageCfgs = messageCfgs;
            }

            /* DEBUG */
            Logger.log(toLogLine('threadCfg: ' + Utilities.jsonStringify(threadCfg)));
            /* DEBUG */
        });
    }

    /* DEBUG */
    Logger.log(toLogLine('threadMessagesToScriptDb.length ' + threadMessagesToScriptDb));
    /* DEBUG */

    return threadMessagesToScriptDb;
}

function sourceMessages(forwardsLim, enforcedMax) {

    Logger.log('forwardsLim:' + forwardsLim);
    Logger.log('enforcedMax:' + enforcedMax);
    var allItems = getScriptDbThreads({
        message_count: db.lessThanOrEqualTo(enforcedMax),
        sys_dirty: true
    }, {
        limit: forwardsLim,
        sortProperty: 'message_count',
        sortDirection: db.DESCENDING
    });

    var i;
    var j;
    var threadCfgs = [];
    var totalMessageCount = 0;
    for (i = 0, j = allItems.length; forwardsLim > 0 && i < j; i++) {
        var thread = GmailApp.getThreadById(allItems[i].item.thread_id);
        var messageCount = thread.getMessageCount();

        if (messageCount != allItems[i].item.message_count) {
            replaceScriptDbThread({
                thread_id: thread.getId(),
                message_count: messageCount,
                sys_dirty: true
            });
            continue;
        } else {
            var messageCfgs = getScriptDbMessages({
                thread_id: allItems[i].item.thread_id
            });

            if (messageCfgs.length != messageCount) {
                throw Error('Alg. error msgCfgs.length ' + messageCfgs.length + ' != thMsgCount ' + messageCount + ' (thread id ' + allItems[i].item.thread_id + ')');
            } else {
                /* DEBUG */
                Logger.log(toLogLine('msgCfgs.length ' + messageCfgs.length + ' == thMsgCount ' + messageCount));
                /* DEBUG */
            }

            threadCfgs.push({
                item: allItems[i].item,
                rawItem: allItems[i].rawItem,
                thread: thread,
                messageCfgs: messageCfgs
            });
            forwardsLim -= messageCount;
            totalMessageCount += messageCount;
        }
    }

    /* DEBUG */
    Logger.log('retrieved threads with ' + totalMessageCount + ' msgs');
    /* DEBUG */

    return threadCfgs;
}
