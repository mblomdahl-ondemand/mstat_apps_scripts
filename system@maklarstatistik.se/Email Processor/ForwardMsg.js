/**
 * File: /mstat_apps_scripts/system@maklarstatistik.se/Email Processor/ForwardMsg.js
 * Author: Mats Blomdahl
 * Version: 0.2
 */


function forwardMessages(threadCfgs) {
    var forwardedStats = {};

    threadCfgs.forEach(function(threadCfg) {

        var removeLabel = GmailApp.getUserLabelByName(threadCfg.item.remove_label);
        var addLabel = GmailApp.getUserLabelByName(threadCfg.item.add_label);
        var forwardTo = threadCfg.item.forward_to;

        // TODO(mats.blomdahl@gmail.com): No more re-sending individual thread messages plz.
        threadCfg.messageCfgs.forEach(function(messageCfg) {

            var message = GmailApp.getMessageById(messageCfg.item.message_id);

            Utilities.sleep(100);

            var attachments = message.getAttachments();

            Utilities.sleep(100);

            if (!message) {
                throw Error('Db error message id ' + messageCfgs.item.message_id + ' not retrieved');
            }

            var fOpts = {
                subject: message.getSubject() + toUsecIdPostfix(message),
                attachments: attachments
            };

            fOpts.attachments.push(getMetaAttachment(getMessageMeta(message, attachments, threadCfg.thread)));

            Utilities.sleep(100);

            message.forward(forwardTo, fOpts);

            Utilities.sleep(500);

            message.markRead();

            Logger.log('sent msg id ' + messageCfg.item.message_id);

            var replacementCfg = replaceScriptDbMessage({
                message_id: messageCfg.item.message_id,
                sys_dirty: false
            });

            if (!replacementCfg) {
                throw Error('Bs replacement of msg id ' + messageCfg.item.message_id + ' failed');
            }

            if (forwardedStats[forwardTo]) {
                forwardedStats[forwardTo]++;
            } else {
                forwardedStats[forwardTo] = 1;
            }

        });

        Utilities.sleep(1500);

        threadCfg.thread.addLabel(addLabel);

        Utilities.sleep(1500);

        threadCfg.thread.removeLabel(removeLabel);

        var replacementCfg = replaceScriptDbThread({
            thread_id: threadCfg.item.thread_id,
            sys_dirty: false
        });

        if (!replacementCfg) {
            throw Error('Bs replacement of thread id ' + threadCfg.item.thread_id + ' failed');
        }

    });

    return forwardedStats;
}
