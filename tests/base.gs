/**
 * @author Mats Blomdahl <mats.blomdahl@gmail.com>
 * @name base.gs
 * @module tests
 * @version 0.4
 * @fileoverview Stub.
 */

/**
 * Plain library import and init test.
 */
function testMstatLibraryImport() {

    mstat.init('testProject', ScriptDb.getMyDb(), 'testProject', true);
}

/**
 * Test ScriptProperties write using the {@code mstat} library.
 */
function testMstatScriptPropStats() {

    /** @typedef {datasource_.ScriptProp} */
    mstat.datasource.ScriptProp.updateForwardedDailyStats({
        'addr1': 3,
        'addr2': 2,
        'addr3': 1
    });

    mstat.datasource.ScriptProp.updateScriptDbIngestionsDailyStats(+2);

    mstat.datasource.ScriptProp.pushPropertyState(true);
}

/**
 * Test scriptDb interface using the {@code mstat.datasource.addThread} method.
 *
 * @returns {Object|false} {@code add_} op. results.
 */
function testMStatScriptDbAddThread(opt_threadId) {
    opt_threadId = opt_threadId || 'foo';
    var result = mstat.datasource.ScriptDb.addThread({thread_id: opt_threadId});
    Logger.log('mstat.datasource.ScriptDb.addThread: Result value \'' + result + '\'.');
    return result;
}

/**
 * Test scriptDb interface using the {@code mstat.datasource.addMessage} method.
 *
 * @param {string=} opt_messageId Stub.
 * @returns {Object|false} {@code add_} op. results.
 */
function testMStatScriptDbAddMessage(opt_messageId) {
    opt_messageId = opt_messageId || 'foo';
    var result = mstat.datasource.ScriptDb.addThread({thread_id: opt_messageId});
    Logger.log('mstat.datasource.ScriptDb.addMessage: Result value \'' + result + '\'.');
    return result;
}

/**
 * Test scriptDb interface {@code @code mstat.datasource.replaceThread} method.
 *
 * @param {Object} oldRecord
 * returns {Object} Write op. result.
 */
function testMStatScriptDbReplaceThread(newRecord) {
    var result = mstat.datasource.ScriptDb.replaceThread(newRecord);
    Logger.log('mstat.datasource.ScriptDb.replaceMessage: Result value \'' + result + '\'.');
    return result;
}

/**
 * Test scriptDb interface using the {@code mstat.datasource.replaceMessage} method.
 *
 * @param {Object} oldRecord
 * returns {Object} Write op. result.
 */
function testMStatScriptDbReplaceMessage(newRecord) {
    var result = mstat.datasource.ScriptDb.replaceMessage(newRecord);
    Logger.log('mstat.datasource.ScriptDb.replaceMessage: Result value \'' + result + '\'.');
    return result;
}

/**
 * Project {@code mstatLibrary} import and aliasing.
 *
 * @type {Object}
 */
this.mstat = mstatLibrary.mstat;

function runTests() {
    testMstatLibraryImport();

    testMstatScriptPropStats();

    testMStatScriptDbAddMessage();

    testMStatScriptDbAddThread();
}
