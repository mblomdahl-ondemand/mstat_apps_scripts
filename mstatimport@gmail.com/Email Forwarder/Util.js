/**
 * @author Mats Blomdahl <mats.blomdahl@gmail.com>
 * @name Util.gs
 * @version 0.3
 *
 * @description
 * Utilities for the `Email Forwarder` Apps Scripts project
 */

/**
 * Returns a labels mapping to pass around.
 *
 * Example:
 * <pre>
 * {
 *     capitex: {
 *         queue: {GmailLabel},
 *         forwarded: {GmailLabel},
 *         parent: {GmailLabel}
 *     },
 *     sf: {
 *         queue: {GmailLabel},
 *         forwarded: {GmailLabel},
 *         parent: {GmailLabel}
 *     },
 *     direct: {
 *         queue: {GmailLabel},
 *         forwarded: {GmailLabel},
 *         parent: {GmailLabel}
 *     }
 * }
 * </pre>
 *
 * ## High-level workflow, step-by-step:
 * <ol>
 *     <li>Messages are sorted into different `queue` labels by means of
 *     <ol>
 *         <li>a. the Gmail inbox filters</li>
 *         <li>b. blabla</li>
 *     </ol>
 *     </li>
 *     <li>A process deletes any duplicate messages based on origin message metadata. Unique messages are added to the
 *     `preProcessed` label.</li>
 *     <li>A process reads messages (i.e. threads) from the {@code preProcessed} label and removes any remaining '
 *     queue' labels. The message (i.e. thread) is then added to the `processed` label.</li>
 *     <li>A process reads messages (i.e. threads) from the `processed` label and forwards the message to GAE
 *     ingestion.</li>
 *    <li>The message (i.e. thread) is then added to the `forwarded` label.</li>
 *    <li>A process reads messages (i.e. threads) from the `forwarded` label and clears any remaining `processed`
 *    labels.</li>
 * </ol>
 * @returns {Object} Mapping of labels to different processing stages
 */
function getLabels() {
    var capitexForwardingQueueLabel = GmailApp.getUserLabelByName("capitex-import@maklarstatistik.se/capitex-forwarding-queue");
    var capitexForwarding = GmailApp.getUserLabelByName("capitex-import@maklarstatistik.se");
    var capitexForwardedLabel = GmailApp.getUserLabelByName("capitex-import@maklarstatistik.se/capitex-forwarded");

    var sfForwardingQueueLabel = GmailApp.getUserLabelByName("sf-import@maklarstatistik.se/sf-forwarding-queue");
    var sfForwardedLabel = GmailApp.getUserLabelByName("sf-import@maklarstatistik.se/sf-forwarded");
    var sfForwarding = GmailApp.getUserLabelByName("sf-import@maklarstatistik.se");

    var directForwardingQueueLabel = GmailApp.getUserLabelByName("direct-import@maklarstatistik.se/direct-forwarding-queue");
    var directForwardedLabel = GmailApp.getUserLabelByName("direct-import@maklarstatistik.se/direct-forwarded");
    var directForwarding = GmailApp.getUserLabelByName("direct-import@maklarstatistik.se");

    return {
        capitex: {
            queue: capitexForwardingQueueLabel,
            forwarded: capitexForwardedLabel,
            parent: capitexForwarding
        },
        sf: {
            queue: sfForwardingQueueLabel,
            forwarded: sfForwardedLabel,
            parent: sfForwarding
        },
        direct: {
            queue: directForwardingQueueLabel,
            forwarded: directForwardedLabel,
            parent: directForwarding
        }
    };
}






