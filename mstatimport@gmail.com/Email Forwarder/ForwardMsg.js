/**
 * @author Mats Blomdahl <mats.blomdahl@gmail.com>
 * @name ForwardMsg.gs
 * @version 0.3
 *
 * @description
 * Processes for performing email forwarding for the `Email Forwarder` Apps Scripts project
 */

/**
 * # Performs forwarding of the messages contained within a list of thread configurations.
 *
 * `ScriptDb` records of type `message` are omitted if their `sys_dirty` property have already been set to `false`.
 * (This state generally occurs after runtime environment failure during execution.)
 *
 * @param Object[] threadCfgs
 * @returns {{}}
 */
function forwardMessages(threadCfgs) {
    // Keeps track of how many messages have been imported to different imports
    var forwardedStats = {};

    threadCfgs.forEach(function(threadCfg) {

        var removeLabel = GmailApp.getUserLabelByName(threadCfg.item.remove_label);
        var addLabel = GmailApp.getUserLabelByName(threadCfg.item.add_label);
        var forwardTo = threadCfg.item.forward_to;

        threadCfg.messageCfgs.forEach(function(messageCfg) {

            if (messageCfg.item.sys_dirty === false) {
                Logger.log('Forwarding of message_id \'' + messageCfg.item.message_id + '\' omitted (sys_dirty=false)');
                return false;
            }

            var message = GmailApp.getMessageById(messageCfg.item.message_id);

            // Avoids Gmail `rateMax` exception for API calls
            Utilities.sleep(100);

            if (!message) {
                throw Error('Error reading message_id \'' + messageCfgs.item.message_id + '\' from Gmail');
            }

            var attachments = message.getAttachments();

            // Avoids Gmail `rateMax` exception for API calls
            Utilities.sleep(100);

            // Forwarding options config -- allows for modifications to subject line and message attachments
            var fOpts = {
                subject: message.getSubject() + toUsecIdPostfix(message),
                attachments: attachments
            };

            // Adds the ``x-mstat-message_meta.json`` attachment to forwarding op.
            fOpts.attachments.push(getMetaAttachment(getMessageMeta(message, attachments, threadCfg.thread)));

            // TODO: Deprecate
            for (var i = fOpts.attachments.length; i--;) {
                Logger.log('forwarding attachment ' + fOpts.attachments[i]);
            }

            // Avoids Gmail `rateMax` exception for API calls
            Utilities.sleep(100);

            message.forward(forwardTo, fOpts);

            // Avoids Gmail `rateMax` exception for API calls
            Utilities.sleep(500);

            message.markRead();

            Logger.log('sent msg id ' + messageCfg.item.message_id);

            var replacementCfg = replaceScriptDbMessage({
                message_id: messageCfg.item.message_id,
                sys_dirty: false
            });

            if (!replacementCfg) {
                throw Error('Updating ScriptDb record for message_id \'' + messageCfg.item.message_id + '\' failed');
            }

            if (forwardedStats[forwardTo]) {
                forwardedStats[forwardTo]++;
            } else {
                forwardedStats[forwardTo] = 1;
            }

        });

        // Avoids Gmail `rateMax` exception for API calls
        Utilities.sleep(1500);

        // Adds thread to the `forwarded` label
        threadCfg.thread.addLabel(addLabel);

        // Avoids Gmail `rateMax` exception for API calls
        Utilities.sleep(1500);

        // Removes thread from the `queue` label
        threadCfg.thread.removeLabel(removeLabel);

        // Updates
        var replacementCfg = replaceScriptDbThread({
            thread_id: threadCfg.item.thread_id,
            sys_dirty: false
        });

        if (!replacementCfg) {
            throw Error('Updating ScriptDb record for thread_id \'' + threadCfg.item.thread_id + '\' failed');
        }

    });

    return forwardedStats;
}
