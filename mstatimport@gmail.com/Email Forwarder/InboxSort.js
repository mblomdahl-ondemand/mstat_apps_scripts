/**
 * File: /mstat_apps_scripts/mstatimport@gmail.com/Email Forwarder/InboxSort.js
 * Author: Mats Blomdahl
 * Version: 0.2
 */


function sortInbox(labels, maxThreads) {
    var inboxOffset = Math.max(GmailApp.getInboxUnreadCount() - maxThreads, 0);

    /* DEBUG */
    Logger.log('inboxOffset: '+inboxOffset);
    /* DEBUG */

    var inboxThreads = GmailApp.getInboxThreads(inboxOffset, maxThreads);
    var messagesSorted = 0;
    var threadsSorted = 0;
    for (var i = 0; i < inboxThreads.length; i++) {
        var threadMessages = inboxThreads[i].getMessages();
        if (threadMessages[0].getFrom() == "maklarstat@scb.se") {
            inboxThreads[i]
                .markUnread()
                .addLabel(labels.capitex.parent)
                .addLabel(labels.capitex.queue)
                .moveToArchive();
        } else {

            /* DEBUG */
            Logger.log('getFrom:' + threadMessages[0].getFrom());
            /* DEBUG */

            if (threadMessages[0].getAttachments().length && threadMessages[0].getSubject().toLowerCase().search('statistik') > -1) {
                inboxThreads[i]
                    .markUnread()
                    .addLabel(labels.direct.parent)
                    .addLabel(labels.direct.queue)
                    .moveToArchive();
            } else {
                continue;
            }
        }
        threadsSorted++;
        threadMessages.forEach(function(message) {
            message.markUnread();
            messagesSorted++;
        });
    }

    /* DEBUG */
    Logger.log('sort(msg): ' + messagesSorted + ' / sort(thr): ' + threadsSorted);
    /* DEBUG */

    return {
        messages: messagesSorted,
        threads: threadsSorted
    };
}
