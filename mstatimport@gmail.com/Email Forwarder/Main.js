/**
 * @author Mats Blomdahl <mats.blomdahl@gmail.com>
 * @name Main.gs
 * @version 0.3
 *
 * @description
 * `Email Forwarder` Apps Scripts project start-up hooks / `main`
 */

// Global variables
var debug = false;
var db = ScriptDb.getMyDb();
var labels = getLabels();

/**
 * # Stub
 *
 * @returns {*}
 */
function run() {
    // TODO: Update `run` to use propertydb for saving state. I.e. successful completion of prep -> run forwarding (and the other way around).
    var now = new Date();
    Logger.log(parseInt(now.getMinutes()));
    if (parseInt(now.getMinutes()) % 2 == 0) {
        return runForwardingOp();
    } else {
        return runPrepOp();
    }
    return;
    // Process prioritized mail
    var quotas = getAvailableQuota();
    if (quotas.forwardsLim) {
        // Sort mail into `capitex` or `direct` labels
        var sortDetails = sortInbox(labels, 23);

        // Source threads into ScriptDb
        updateScriptDbIngestionsDailyStats(sourceThreads(12, true)); //,quotas.forwardsLim ['capitex', 'sf']

        pushPropertyState(true);

        // Source messages from ScriptDb
        var threadCfgs = sourceMessages(quotas.forwardsLim, quotas.enforcedMax);
        updateForwardedDailyStats(forwardMessages(threadCfgs));

        pushPropertyState(true);
    }
}

/**
 * Stub.
 *
 */
function runForwardingOp() {
    var quotas = getAvailableQuota();
    Logger.log('quotas.forwardsLim: ' + quotas.forwardsLim);
    if (quotas.forwardsLim) {

        // Source messages from ScriptDb
        var threadCfgs = sourceMessages(quotas.forwardsLim, quotas.enforcedMax);

        updateForwardedDailyStats(forwardMessages(threadCfgs));

        pushPropertyState(true);
    }
}

/**
 * Stub.
 *
 */
function runPrepOp() {
    var quotas = getAvailableQuota();
    Logger.log('quotas.forwardsLim: ' + quotas.forwardsLim);
    // Sort mail into `capitex` or `direct` labels
    var sortDetails = sortInbox(labels, 23);

    // Source threads into ScriptDb
    updateScriptDbIngestionsDailyStats(sourceThreads(23, true));

    pushPropertyState(true);
}

/**
 * Stub.
 *
 */
function test() {
    /*_flushScriptDb(true);
     return;*/
    var result = db.query({t: 'T_M', tid: '1354051deb92bebf'});
    Logger.log(result.getSize());
    var ids = [];
    var dup = [];
    while (result.hasNext()) {
        var item = result.next();
        var id = item.mid;
        if (ids.indexOf(id) < 0) {
            ids.push(id);
        } else {
            if (dup.indexOf(id) < 0) {
                dup.push(id);
            }
        }
    }
    for (var i = dup.length; i--;) {
        var result = db.query({t: 'T_M', tid: '1354051deb92bebf', mid: dup[i], df: false});
        db.remove(result.next());
    }
    Logger.log(ids.length);
    Logger.log(dup.length);
    return;
    Logger.log('db.lessThanOrEqualTo: ' + db.lessThan(43));
    var result = db.query({t: 'thread', cnt: db.between(0, 43) });
    if (result.hasNext()) {
        Logger.log('result: ' + Utilities.jsonStringify(result.next()));
    }

    var inboxThreads = GmailApp.getInboxThreads(0, 2);
    for (var i = inboxThreads.length; i--;) {
        inboxThreads[i].getMessages().forEach(function(message) {
            Logger.log('msg id:' + message.getId());
        });
    }

    /*var statsMap = {
     'capitex-import@maklarstatistik.se': 0,
     'sf-import@maklarstatistik.se': 0,
     'direct-import@maklarstatistik.se': 0
     };
     updateStats(statsMap);*/
}
