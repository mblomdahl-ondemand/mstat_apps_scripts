/**
 * @author Mats Blomdahl <mats.blomdahl@gmail.com>
 * @name SourceMsg.gs
 * @version 0.3
 *
 * @description
 * Processes for sourcing threads/messages used by the `Email Forwarder` Apps Scripts project
 */

/**
 * # Stub.
 *
 * @param {Number} batchMaxTotal
 * @param {String[]} excludeCategories
 * @returns {{ labelConfigs: Array, totalReadCount: Number }}
 * @private
 */
function _balanceLabelReads(batchMaxTotal, excludeCategories) {
    var labelConfigs = [];
    var labelCategory, readCount, unreadCount, i;

    for (labelCategory in labels) {
        if (excludeCategories.indexOf(labelCategory) !== -1) {
            // Omit excluded label category from sourcing op
            continue;
        }
        unreadCount = labels[labelCategory].queue.getUnreadCount();
        Utilities.sleep(1e3);
        if (unreadCount) {
            labelConfigs.push([labelCategory, unreadCount]);

            /* DEBUG */
            Logger.log('Found ' + unreadCount + ' unread for label category ' + labelCategory);
            /* DEBUG */
        }
    }

    // Attempt even distribution across import paths
    var threadsPerLabel = parseInt(batchMaxTotal / labelConfigs.length);
    var rest = batchMaxTotal % labelConfigs.length;
    for (i = labelConfigs.length; i--;) {
        unreadCount = labelConfigs[i][1];
        readCount = threadsPerLabel;
        if (rest && (readCount + rest) <= unreadCount) {
            readCount += rest;
            rest = 0;
        } else if (readCount > unreadCount) {
            readCount = unreadCount;
            rest += unreadCount - threadsPerLabel;
        }
        labelConfigs[i].push(readCount);
    }

    var totalReadCount = 0;

    for (i = labelConfigs.length; rest && i--;) {
        unreadCount = labelConfigs[i][1];
        readCount = labelConfigs[i][2];
        if ((readCount + rest) <= unreadCount) {
            labelConfigs[i][2] += rest;
            rest = 0;
        } else {
            var countDx = unreadCount - readCount;
            labelConfigs[i][2] += countDx;
            rest -= countDx;
        }
        totalReadCount += labelConfigs[i][2];
    }

    /* DEBUG */
    if (debug) {
        var tpl = 'Label Category: %s / Unread: %i / ToRead: %i';
        for (i = labelConfigs.length; i--;) {
            Logger.log(Utilities.formatString(tpl, labelConfigs[i][0], labelConfigs[i][1], labelConfigs[i][2]));
        }
    }
    /* DEBUG */

    if (totalReadCount > batchMaxTotal) {
        throw Error('Alg. error totalRC ' + totalReadCount + ' > maxTh ' + batchMaxTotal);
    } else {
        /* DEBUG */
        if (debug) {
            Logger.log(toLogLine('totalRC ' + totalReadCount + ' <= maxTh ' + batchMaxTotal));
        }
        /* DEBUG */
    }

    return {
        labelConfigs: labelConfigs,
        totalReadCount: totalReadCount
    };
}

/**
 * # Reads threads range from the target label and calls `forEachCallback(thread)`
 *
 * @param {GmailLabel} label
 * @param {Number} maxThreads
 * @param {Function} forEachCallback
 * @param {Boolean} readDescending
 * @private
 */
function _getLabelThreads(label, maxThreads, forEachCallback, readDescending) {
    readDescending = readDescending || false;

    // Max label threads available
    var unreadCount = label.getUnreadCount();

    // Label threads read offset (start from the bottom)
    var threadsReadOffset = 0;
    if (!readDescending) {
        threadsReadOffset = Math.max(unreadCount - maxThreads, 0);
    }

    label.getThreads(threadsReadOffset, maxThreads).forEach(forEachCallback);
}

/**
 * # Reads threads from the `processingQueue` label and performs initial ScriptDb ingestion
 *
 * @param {Number} forwardsLim
 * @param {Boolean|} readDescending
 * @param {String[]|} excludeCategories
 * @returns {Number}
 */
function sourceThreads(forwardsLim, readDescending, excludeCategories) {
    readDescending = readDescending || false;
    excludeCategories = excludeCategories || [];

    var readConfig = _balanceLabelReads(forwardsLim, excludeCategories);
    var labelConfigs = readConfig.labelConfigs;
    var totalReadCount = readConfig.totalReadCount;

    // Load and submit threads to `ScriptDb` queue
    var i, threadMessagesToScriptDb = 0;
    for (i = labelConfigs.length; i--;) {

        var threadMaxReadCount = labelConfigs[i][2];
        if (!threadMaxReadCount) {
            continue;
        }

        // The `processingQueue` label; where inbound messages arrive prior to processing
        var fromLabel = labels[labelConfigs[i][0]].queue;
        var fromLabelName = fromLabel.getName();

        // The `forwarded` label; where inbound messages are stored post-forwarding
        var toLabelName = labels[labelConfigs[i][0]].forwarded.getName();

        // Target system email, defines origin and import path
        var forwardTo = labels[labelConfigs[i][0]].parent.getName();

        _getLabelThreads(fromLabel, threadMaxReadCount, function(thread) {
            var messageCount = thread.getMessageCount();
            var threadId = thread.getId();
            var messageCfgs = [];

            thread.getMessages().forEach(function(message) {

                var cfg = addScriptDbMessage({
                    thread_id: threadId,
                    message_id: message.getId(),
                    forward_to: forwardTo,
                    sys_dirty: true
                });

                if (cfg) {
                    messageCfgs.push(cfg);
                    threadMessagesToScriptDb++;
                }

            });

            var cfg = {
                thread_id: threadId,
                message_count: messageCount,
                remove_label: fromLabelName,
                add_label: toLabelName,
                forward_to: forwardTo,
                sys_dirty: true
            };

            var threadCfg = addScriptDbThread(cfg);

            if (messageCfgs.length && !threadCfg) { // threadCfg dirty
                threadCfg = replaceScriptDbThread(cfg);
                if (!threadCfg) {
                    throw Error('ScriptDb thread replacement failure for thread_id ' + threadId);
                }
            }

            if (threadCfg) {
                threadCfg.messageCfgs = messageCfgs;
            }

            /* DEBUG */
            Logger.log(toLogLine('threadCfg: ' + Utilities.jsonStringify(threadCfg)));
            /* DEBUG */
        }, readDescending);
    }

    /* DEBUG */
    Logger.log(toLogLine('threadMessagesToScriptDb.length ' + threadMessagesToScriptDb));
    /* DEBUG */

    return threadMessagesToScriptDb;
}

/**
 * # Retrieves ScriptDb `thread`-type records where `sys_dirty` flag is set to `true`
 *
 * @param {Number} forwardsLim Target batch size
 * @param {Number} enforcedMax Hard upper limit for batch size
 * @returns {{ item: Object, rawItem: Object, thread: GmailThread, messageCfgs: { item: Object, rawItem: Object }[] }[]}
 *     Array of thread configurations along with descendent message configs.
 */
function sourceMessages(forwardsLim, enforcedMax) {

    Logger.log('forwardsLim:' + forwardsLim);
    Logger.log('enforcedMax:' + enforcedMax);
    var allItems = getScriptDbThreads({
        message_count: db.lessThanOrEqualTo(enforcedMax),
        sys_dirty: true
    }, {
        limit: forwardsLim,  // Assumes each thread contain at least 1 message
        sortProperty: 'message_count',
        sortDirection: db.DESCENDING  // db.ASCENDING
    });

    var threadCfgs = [];
    var totalMessageCount = 0;
    for (var i = 0, j = allItems.length; forwardsLim > 0 && i < j; i++) {
        var thread = GmailApp.getThreadById(allItems[i].item.thread_id);
        var threadMessageCount = thread.getMessageCount();

        if (threadMessageCount != allItems[i].item.message_count) {
            Logger.log('thread_id \'' + allItems[i].item.thread_id + '\' omitted from sourcing op (allItems[i].item.message_count \'' + messageCfgs.length + '\' != threadMessageCount \'' + threadMessageCount + '\'.');
            replaceScriptDbThread({
                thread_id: thread.getId(),
                message_count: threadMessageCount,
                sys_dirty: true
            });
            // continue;
        } else {
            // Queries for all ScriptDb `message`-type records associated with the thread
            var messageCfgs = getScriptDbMessages({
                thread_id: allItems[i].item.thread_id
            });

            if (messageCfgs.length != threadMessageCount) {
                Logger.log('thread_id \'' + allItems[i].item.thread_id + '\' omitted from sourcing op (messageCfgs.length \'' + messageCfgs.length + '\' != threadMessageCount \'' + threadMessageCount + '\'.');
                continue;  // TODO(mats.blomdahl@gmail.com): Replace with alg. for selecting remaining messages _only_.
                // throw Error('Alg. error msgCfgs.length ' + messageCfgs.length + ' != thMsgCount ' + threadMessageCount + ' (thread_id ' + allItems[i].item.thread_id + ', msgCfgs: "' + Utilities.jsonStringify(messageCfgs) + '")');
            } else {
                /* DEBUG */
                Logger.log(toLogLine('msgCfgs.length ' + messageCfgs.length + ' == thMsgCount ' + threadMessageCount));
                /* DEBUG */
            }

            threadCfgs.push({
                item: allItems[i].item,
                rawItem: allItems[i].rawItem,
                thread: thread,
                messageCfgs: messageCfgs
            });
            forwardsLim -= threadMessageCount;
            totalMessageCount += threadMessageCount;
        }
    }

    /* DEBUG */
    Logger.log('retrieved threads with ' + totalMessageCount + ' msgs');
    /* DEBUG */

    return threadCfgs;
}
