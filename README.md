# _mstat\_apps\_scripts_ Project

The _mstat\_apps\_scripts_ project consists of a core JavaScript library, `mstat`, and two Gmail-centric Google Apps Scripts implementations.

The main aim for the project is to provide a rich foundation for Apps Scripts development and to mitigate the effects of Google's horribly unstable runtime environment.


## 1. Components


### 1.1. Core Library [draft]

Component Root: `/mstat`

Script Project: [https://script.google.com/a/maklarstatistik.se/d/1LU5Y...](https://script.google.com/a/maklarstatistik.se/d/1LU5YLozLL5oa8YBmJQa5TTFgQBwoPkBtJ00zr8FoU08K0QugYOOwEACL/edit?usp=sharing)

Project Key: `M4Nnc8axZItUJfdksl1DS-Ucg7Il6qISr`

_To be continued..._


#### 1.1.1. UnderscoreGS [draft]

Project Info: https://sites.google.com/site/scriptsexamples/custom-methods/underscoregs

Project Key: `MiC3qjLYVUjCCUQpMqPPTWUF7jOZt2NQ8`

_To be continued..._

#### 1.1.2. Library Import

How to import the `mstat` namespace into your script:

```
#!javascript
this.mstat = mstatLibrary.mstat; // within the global context
mstat.init('myProject', ScriptDb.getMyDb(), 'myScriptProp');
```


### 1.2. Library Tests [draft]

Component Root: `/tests`

Script Project: [https://script.google.com/a/maklarstatistik.se/d/1Lo94...](https://script.google.com/a/maklarstatistik.se/d/1Lo942vL1UHzLX29XMvse3sgvACpGbKGXgA4tZScT-fgsq-FMqa560GRR/edit?usp=sharing)

Project Key: `MaWeawOgh2XZN4b_Or4z7RjIqpxakm-Wx`

_To be continued..._


### 1.3. _mstatimport@gmail.com_ Package [draft]

Package Root: `/mstatimport@gmail.com`

_To be continued..._


#### 1.3.1. The _Email Forwarder_ Application [draft]

Component Root: `/mstatimport@gmail.com/Email Forwarder`

_To be continued..._


### 1.4. _system@maklarstatistik.se_ Package [draft]

Package Root: `/system@maklarstatistik.se`

_To be continued..._


#### 1.4.1. The _Email Processor_ Application [draft]

Component Root: `/system@maklarstatistik.se/Email Processor`

_To be continued..._


## 2. Development Env. [draft]

Short version: It's crap. 

_To be continued..._


## 3. Deployment

Deployment is far from elegant at this stage. We actually have to 

1. open the target project in the Google Drive _Script Editor_ app
2. walk through each of changed script files and copy-paste from the repo source code
3. save the project.  

Well, that's our deployment. :-) Please make use of the built-in Google Apps Scripts versioning tools for any non-trivial changes.


## 4. Code Style

For project code style, refer to the [Google JavaScript Style Guide](http://google-styleguide.googlecode.com/svn/trunk/javascriptguide.xml).

Comments are written in [JSDoc](http://usejsdoc.org/)-style, though we do our best to minimize use of tags explicitly discouraged by the Google style guide.

Line-width: 120 ch.

Line separator: Always use LF (\n), never CRLF

Indentation: Always use 4-space indents, never tabs (\t)


## 5. Known Issues [draft]

Many:

* ...

_To be continued..._


## 5. Contact
For general inquiries, security issues and feature requests, contact lead developer [Mats Blomdahl](mailto:mats.blomdahl@gmail.com).
