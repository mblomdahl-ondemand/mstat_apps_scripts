/**
 * @author Mats Blomdahl <mats.blomdahl@gmail.com>
 * @name util_.gs
 * @module mstat.util
 * @version 0.4
 * @fileoverview Common utilities used within the Google Enterprise Account of Svensk Mäklarstatistik AB.
 *
 * Most notably used by the <em>Email Forwarder</em> and <em>Email Processor</em> Apps Scripts projects.
 */

// TODO(mats.blomdahl@gmail.com): Resolve ´backward´ dependency mgnt. Less hacks plz.

/**
 * Default JSON attachment name for message metadata.
 *
 * @type {string}
 * @const
 * @private
 */
META_ATTACHMENT_NAME_ = 'x-mstat-message_meta.json';

/**
 * Implementation of the {@code mstat.util} namespace.
 *
 * @type {Object}
 * @const
 * @private
 */
var util_ = {};

/**
 * Drop-in replacement for Google Apps Scripts {@code Utilities.sleep).
 *
 * @param {number=} opt_time Sleep time (ms).
 * @returns {void} Nothing.
 */
util_.sleep = function(opt_time) {
    opt_time = opt_time || 250;

    Utilities.sleep(opt_time);
};

/**
 * Simple utility method for prefixing log-lines.
 *
 * @param {string} logLine
 * @returns {string} The prefixed log-line
 */
util_.toLogLine = function(logLine) {
    var formattedDate = Utilities.formatDate(new Date(), "UT", "yy-MM-dd'T'HH:mm:ss");
    return Utilities.formatString('[%s] %s', formattedDate, logLine);
};

/**
 * Generates a unique identifier for an outgoing message.
 *
 * @param {GmailMessage} message A Gmail message instance.
 * @returns {string} A unique and sequential message identifier.
 */
util_.toUsecIdPostfix = function(message) {
    return Utilities.formatString(' ###MSEC:%i#ID:%s###', new Date().valueOf(), message.getId());
};

/**
 * ISO date-formatting function
 *
 * @param {Date} date Native JavaScript datetime instance
 * @returns {string} ISO format date string
 */
util_.toIsoFormat = function(date) {
    return Utilities.formatDate(date, "UT", "yyyy-MM-dd'T'HH:mm:ss");
};

/**
 * Default callback on exhausted MailApp API quota.
 *
 * @param {number} remainingQuota The amount of MailApp send-mail quota remaining.
 * @throws {Error} When MailApp API quota is exhausted.
 * @private
 */
function defaultLowQuotaCallback_(remainingQuota) {
    var logLine = util_.toLogLine('Execution omitted due to insufficient quota');
    if (remainingQuota == 1) {
        GmailApp.sendEmail('mats.blomdahl@gmail.com', 'mstat.util.getAvailableSendMailQuota: Error', logLine);
    } else {
        Logger.log(logline);
    }
    throw Error('MailApp API quota exhausted');
}

/**
 * Checks for remaining forwarding quota with the MailApp API.
 *
 * @param {number=} opt_reservedQuota Reserved quota to deduct from response, defaults to 1.
 * @param {function(?)=} opt_lowQuotaCallback Callback function on critical low, defaults to
 *                                            {@code defaultLowQuotaCallback_}.
 * @returns {number} Hard limit on send-mail quota.
 *
 */
util_.getAvailableSendMailQuota = function(opt_reservedQuota, opt_lowQuotaCallback) {
    opt_reservedQuota = opt_reservedQuota || 1;
    opt_lowQuotaCallback = opt_lowQuotaCallback || defaultLowQuotaCallback_;

    var remainingQuota = MailApp.getRemainingDailyQuota();

    if (remainingQuota > opt_reservedQuota) {
        return remainingQuota - opt_reservedQuota;
    } else {
        opt_lowQuotaCallback(remainingQuota);
        return remainingQuota - opt_reservedQuota;
    }
};

/**
 * Extracts message metadata into standard JavaScript object.
 *
 * Sample response:
 * <pre>
 * {
 *     to: {string|null},
 *     from: {string|null},
 *     cc: {string|null},
 *     bcc: {string|null},
 *     replyTo: {string|null},
 *     date: {string|null},
 *     subject: {string|null},
 *     body: {string|null},
 *     plainBody: {string|null},
 *     messageId: {string|null},
 *     attachmentMeta: {Array.<string>|null},
 *     threadId: {string|null},
 *     threadPermalink: {string|null}
 * }
 * </pre>
 *
 * @param {GmailMessage} message Target Gmail message.
 * @param {Array.<GmailAttachment>} attachments The Gmail message attachments.
 * @param {GmailThread} thread The message's parent Gmail thread.
 * @returns {Object} The extracted attachment metadata.
 */
util_.getMessageMeta = function(message, attachments, thread) {
    var attachmentMeta = [];
    for (var i = 0, j = attachments.length; i < j; i++) {
        attachmentMeta.push({
            size: attachments[i].getSize(),
            filename: attachments[i].getName()
        });
        // TODO: Deprecate.
        Logger.log('attachment processed');
    }

    // Avoids the Gmail 'rateMax' exception on API calls
    Utilities.sleep(500);

    return {
        to: message.getTo(),
        from: message.getFrom(),
        cc: message.getCc(),
        bcc: message.getBcc(),
        replyTo: message.getReplyTo(),
        date: mstat.util_.toIsoFormat(message.getDate()),
        subject: message.getSubject(),
        body: message.getBody(),
        plainBody: message.getPlainBody(),
        messageId: message.getId(),
        attachmentMeta: attachmentMeta,
        threadId: thread.getId(),
        threadPermalink: thread.getPermalink()
    };
};

/**
 * Generates JSON attachment from message metadata.
 *
 * @param {Object} messageMeta Message metadata object.
 * @param {string=} opt_attachmentName Attachment file name, defaults to {@code META_ATTACHMENT_NAME_}.
 * @returns {Blob} JSON attachment blob.
 */
util_.getAttachmentFromMeta = function(messageMeta, opt_attachmentName) {
    opt_attachmentName = opt_attachmentName || META_ATTACHMENT_NAME_;
    return Utilities.newBlob(Utilities.jsonStringify(messageMeta), 'application/json', opt_attachmentName);
};

/**
 * Extracts message metadata from an inbound email.
 *
 * Example response:
 * <pre>
 * {
 *     thread_id: {string},
 *     message_id: {string},
 *     date: {string}
 * }
 * </pre>
 *
 * @param {Array.<GmailAttachment>} attachments The Gmail message attachments.
 * @param {string=} opt_attachmentName Attachment file name, defaults to {@code META_ATTACHMENT_NAME_}.
 * @returns {Object} Object containing the parsed message metadata.
 * @throws {Error} When no attachments matches filename {@code opt_attachmentName}.
 */
util_.getMetaFromAttachments = function(attachments, opt_attachmentName) {
    opt_attachmentName = opt_attachmentName || META_ATTACHMENT_NAME_;
    for (var i = attachments.length; i--;) {
        if (attachments[i].getName() == opt_attachmentName) {
            var metaAttachment = Utilities.jsonParse(attachments[i].getDataAsString());
            return {
                thread_id: metaAttachment.threadId,
                message_id: metaAttachment.messageId,
                date: metaAttachment.date
            };
        }
    }
    throw Error('Unable to identify \'' + opt_attachmentName + '\' meta attachment');
};

mstat.scope(function() {
    mstat.util = util_;
});
