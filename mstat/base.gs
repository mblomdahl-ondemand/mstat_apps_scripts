/**
 * @author Mats Blomdahl <mats.blomdahl@gmail.com>
 * @name base.gs
 * @module mstat
 * @version 0.4
 * @fileoverview Bootstrap for the Google Apps Scripts {@code mstat} library.
 */

// TODO(mats.blomdahl@gmail.com): Resolve ´backward´ dependency mgnt. Less hacks plz.

/**
 * Base namespace for the {@code mstat} library.
 *
 * @const
 */
var mstat = mstat || {};

/**
 * Closure-inspired reference to the global context.
 */
mstat.global = this;

/**
 * Closure-inspired function returning true if the specified value is defined.
 *
 * @param {*} value Test value.
 * @return {boolean} Whether {@code value} is defined.
 */
mstat.isDef = function(value) {
    return value !== undefined;
};

/**
 * Closure-inspired function returning true if the specified value is null.
 *
 * @param {*} value Test value.
 * @return {boolean} Whether {@code value} is null.
 */
mstat.isNull = function(value) {
    return value === null;
};

/**
 * Closure-inspired function returning true if the specified value is defined and not null.
 *
 * @param {*} value Test value.
 * @return {boolean} Whether {@code value} is defined and not null.
 */
mstat.isDefAndNotNull = function(value) {
    return value != null;
};

/**
 * Closure-inspired function returning true if the object seems to be a JavaScript Date instance.
 *
 * @param {*} value Test value.
 * @return {boolean} Whether {@code value} is a Date (or somewhat similar).
 */
mstat.isDateLike = function(value) {
    return mstat.isObject(value) && typeof value.getFullYear == 'function';
};

/**
 * Closure-inspired function returning true if the specified value is a string.
 *
 * @param {*} value Test value.
 * @return {boolean} Whether {@code value} is a string.
 */
mstat.isString = function(value) {
    return typeof value == 'string';
};

/**
 * Closure-inspired function returning true if the specified value is a boolean.
 *
 * @param {*} value Variable to test.
 * @return {boolean} Whether {@code value} is boolean.
 */
mstat.isBoolean = function(value) {
    return typeof value == 'boolean';
};

/**
 * Closure-inspired function returning true if the specified value is a number.
 *
 * @param {*} value Test value.
 * @return {boolean} Whether {@code value} is a number.
 */
mstat.isNumber = function(value) {
    return typeof value == 'number';
};

/**
 * Closure-inspired function returning true if the specified value is an object (arrays and functions included).
 *
 * @param {*} value Test value.
 * @return {boolean} Whether {@code value} is an object.
 */
mstat.isObject = function(value) {

    var type = typeof value;

    return type == 'object' && value != null || type == 'function';
};

/**
 * {@code DEBUG} is a convenience property enabling verbose logging.
 *
 * @type {boolean}
 * @see {@code mstat.init}
 */
mstat.DEBUG;

/**
 * Closure-inspired scoping function.
 *
 * @param {function()} fn Function to call.
 */
mstat.scope = function(fn) {
    fn.call(mstat.global);
};

/**
 * ExtJS-inspired Apply(-If) function.
 *
 * @param {Object} config The source object / property configuration.
 * @param {Object=} opt_object Target object for applying the new properties or null.
 * @param {boolean=} opt_applyIf Apply-If flag, defaults to false.
 * @returns {Object} The target object or a new object with src properties applied.
 */
mstat.apply = function(config, opt_object, opt_applyIf) {
    opt_applyIf = opt_applyIf || false;

    opt_object = opt_object || {};
    for (var property in config) {
        if ((opt_applyIf && opt_object[property] === undefined) || !opt_applyIf) {
            opt_object[property] = config[property];
        }
    }
    return opt_object;
};

/**
 * Short-hand for {@code apply(<srcConfig>, <targetObject>, true)}.
 *
 * @param {Object} config The source object / property configuration.
 * @param {Object=} opt_object Target object for applying any undefined properties.
 * @returns {Object} The target object with extended properties set.
 */
mstat.applyIf = function(config, opt_object) {
    return mstat.apply(config, opt_object, true);
};

/**
 * Closure-inspired null function.
 *
 * @returns {void} Nothing.
 */
mstat.nullFn = function() {};

/**
 * Closure-inspired identity function. Returns its first argument.
 *
 * @param {*=} opt_value
 * @returns {?} The first argument.
 */
mstat.identityFn = function(opt_value) {
    return opt_value;
};

/**
 * {@code mstat.util} namespace.
 *
 * @type {Object}
 * @const
 */
mstat.util = initUtil();

/**
 * {@code mstat.datasource} namespace.
 *
 * @type {Object}
 * @const
 */
mstat.datasource = {};

/**
 * Library-owned ScriptDb instance.
 *
 * @type {ScriptDb}
 * @const
 */
mstat.datasource.scriptDb = ScriptDb.getMyDb();

/**
 * {@mstat.datasource} ScriptDb wrapper.
 *
 * @type {datasource_.ScriptDb}
 * @const
 * @see {@code mstat.init}
 */
mstat.datasource.ScriptDb;

/**
 * {@mstat.datasource} ScriptProperties wrapper.
 *
 * @type {datasource_.ScriptProp}
 * @const
 */
mstat.datasource.ScriptProp;

/**
 * Flag used for preventing re-initialization.
 *
 * @type {boolean}
 * @private
 */
mstat.INITIALIZED_ = false;

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Stub.
 *
 * @param {string} dbNamespace
 * @param {ScriptDb} dbInstance
 * @param {string} propertyNamespace
 * @param {boolean=} opt_debug
 * @param {function(string, string, number, number, number)=} opt_dailyStatsResetCallback
 */
mstat.init = function(dbNamespace, dbInstance, propertyNamespace, opt_debug, opt_dailyStatsResetCallback) {

    if (mstat.INITIALIZED_) {
        throw Error('Already initialized');
    }

    // Initialize DEBUG config
    mstat.DEBUG = opt_debug || false;

    // Reconfigure library to use caller's ScriptDb instance.
    mstat.datasource.scriptDb = dbInstance;

    // Instantiate library ScriptDb wrapper.
    mstat.datasource.ScriptDb = new datasource_.ScriptDb(dbNamespace);

    // Instantiate library ScriptProperties wrapper.
    mstat.datasource.ScriptProp = new datasource_.ScriptProp(propertyNamespace, opt_dailyStatsResetCallback);

    // Mark library initialization as completed to prevent re-initialization.
    mstat.INITIALIZED_ = true;
};

