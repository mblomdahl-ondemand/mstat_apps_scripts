/**
 * @author Mats Blomdahl <mats.blomdahl@gmail.com>
 * @name datasource.gs
 * @module mstat.datasource
 * @version 0.4
 * @fileoverview Interfaces wrapping the Google Apps Scripts <em>ScriptProperties</em> and <em>ScriptDb</em> services.
 */

// TODO(mats.blomdahl@gmail.com): Resolve ´backward´ dependency mgnt. Less hacks plz.

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Stub.
 *
 * @type {Object.Object.<string>,<string>}
 * @private
 * @const
 */
TYPE_MEMOIZER_MAPPING_ = {
    toBaseValue: {
        'thread': 'T_T',
        'thread-rm': 'T_TD',
        'message': 'T_M',
        'message-rm': 'T_MD'
    },
    toUserValue: {
        T_T: 'thread',
        T_TD: 'thread-rm',
        T_M: 'message',
        T_MD: 'message-rm'
    }
};

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Stub.
 *
 * @type {Object.Object.Object.<string>,<string>}
 * @private
 * @const
 */
LABEL_MEMOIZER_MAPPING_ = {
    removeLabel: {
        toBaseValue: {
            "capitex-import@maklarstatistik.se/capitex-forwarding-queue": 'RL_C',
            "sf-import@maklarstatistik.se/sf-forwarding-queue": 'RL_S',
            "direct-import@maklarstatistik.se/direct-forwarding-queue": 'RL_D',
            "capitex-import@maklarstatistik.se/capitex-processing-queue": 'RL_CQ',
            "sf-import@maklarstatistik.se/sf-processing-queue": 'RL_SQ',
            "direct-import@maklarstatistik.se/direct-processing-queue": 'RL_DQ',
            "capitex-import@maklarstatistik.se/capitex-pre-processed": 'RL_CP',
            "sf-import@maklarstatistik.se/sf-pre-processed": 'RL_SP',
            "direct-import@maklarstatistik.se/direct-pre-processed": 'RL_DP',
            "capitex-import@maklarstatistik.se/capitex-processed": 'RL_CP2',
            "sf-import@maklarstatistik.se/sf-processed": 'RL_SP2',
            "direct-import@maklarstatistik.se/direct-processed": 'RL_DP2'
            },
        toUserValue: {
            RL_C: "capitex-import@maklarstatistik.se/capitex-forwarding-queue",
            RL_S: "sf-import@maklarstatistik.se/sf-forwarding-queue",
            RL_D: "direct-import@maklarstatistik.se/direct-forwarding-queue",
            RL_CQ: "capitex-import@maklarstatistik.se/capitex-processing-queue",
            RL_SQ: "sf-import@maklarstatistik.se/sf-processing-queue",
            RL_DQ: "direct-import@maklarstatistik.se/direct-processing-queue",
            RL_CP: "capitex-import@maklarstatistik.se/capitex-pre-processed",
            RL_SP: "sf-import@maklarstatistik.se/sf-pre-processed",
            RL_DP: "direct-import@maklarstatistik.se/direct-pre-processed",
            RL_CP2: "capitex-import@maklarstatistik.se/capitex-processed",
            RL_SP2: "sf-import@maklarstatistik.se/sf-processed",
            RL_DP2: "direct-import@maklarstatistik.se/direct-processed"
        }
    },
    addLabel: {
        toBaseValue: {
            "capitex-import@maklarstatistik.se/capitex-forwarded": 'AL_C',
            "sf-import@maklarstatistik.se/sf-forwarded": 'AL_S',
            "direct-import@maklarstatistik.se/direct-forwarded": 'AL_D',
            "capitex-import@maklarstatistik.se/capitex-processing-queue": 'AL_CQ',
            "sf-import@maklarstatistik.se/sf-processing-queue": 'AL_SQ',
            "direct-import@maklarstatistik.se/direct-processing-queue": 'AL_DQ',
            "capitex-import@maklarstatistik.se/capitex-pre-processed": 'AL_CP',
            "sf-import@maklarstatistik.se/sf-pre-processed": 'AL_SP',
            "direct-import@maklarstatistik.se/direct-pre-processed": 'AL_DP',
            "capitex-import@maklarstatistik.se/capitex-processed": 'AL_CP2',
            "sf-import@maklarstatistik.se/sf-processed": 'AL_SP2',
            "direct-import@maklarstatistik.se/direct-processed": 'AL_DP2'
        },
        toUserValue: {
            AL_C: "capitex-import@maklarstatistik.se/capitex-forwarded",
            AL_S: "sf-import@maklarstatistik.se/sf-forwarded",
            AL_D: "direct-import@maklarstatistik.se/direct-forwarded",
            AL_CQ: "capitex-import@maklarstatistik.se/capitex-processing-queue",
            AL_SQ: "sf-import@maklarstatistik.se/sf-processing-queue",
            AL_DQ: "direct-import@maklarstatistik.se/direct-processing-queue",
            AL_CP: "capitex-import@maklarstatistik.se/capitex-pre-processed",
            AL_SP: "sf-import@maklarstatistik.se/sf-pre-processed",
            AL_DP: "direct-import@maklarstatistik.se/direct-pre-processed",
            AL_CP2: "capitex-import@maklarstatistik.se/capitex-processed",
            AL_SP2: "sf-import@maklarstatistik.se/sf-processed",
            AL_DP2: "direct-import@maklarstatistik.se/direct-processed"
        }
    }
};

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Stub.
 *
 * @type {Object.Object.Object.<string>,<string>}
 * @private
 * @const
 */
FORWARD_ADDR_MEMOIZER_MAPPING_ = {
    forwardTo: {
        toBaseValue: {
            "capitex-import@maklarstatistik.se": 'FT_C',
            "sf-import@maklarstatistik.se": 'FT_S',
            "direct-import@maklarstatistik.se": 'FT_D'
        },
        toUserValue: {
            FT_C: "capitex-import@maklarstatistik.se",
            FT_S: "sf-import@maklarstatistik.se",
            FT_D: "direct-import@maklarstatistik.se"
        }
    },
    forwardFrom: {
        toBaseValue: {
            "capitex-import@maklarstatistik.se": 'FF_C',
            "sf-import@maklarstatistik.se": 'FF_S',
            "direct-import@maklarstatistik.se": 'FF_D'
        },
        toUserValue: {
            FF_C: "capitex-import@maklarstatistik.se",
            FF_S: "sf-import@maklarstatistik.se",
            FF_D: "direct-import@maklarstatistik.se"
        }
    }
};

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Stub.
 *
 * @param {string} dbProperty ScriptDb property name.
 * @param {string} userProperty User property name.
 * @param {ScriptDb.ENUM=} opt_sortStrategy ScriptDb sort strategy, defaults to
 *                                          {@code mstat.datasource.scriptDb.LEXICAL}.
 * @param {function(?):<string,number>=} opt_toBaseValue Function converting user values to ScriptDb values, defaults to
 *                                                       {@code mstat.identityFn}.
 * @param {function(?):<string,number>=} opt_toUserValue Function converting ScriptDb values back to user values,
 *                                                       defaults to {@code mstat.identityFn}.
 * @param {function()=} opt_autoPopulateBaseValue Function populating undefined values on {@code toBaseValue}
 *                                                conversion.
 * @constructor
 * @private
 */
var ScriptDbMemoizer_ = function(dbProperty, userProperty, opt_sortStrategy, opt_toBaseValue, opt_toUserValue,
                                 opt_autoPopulateBaseValue) {

    // TODO(mats.blomdahl@gmail.com): Complete docs stub.
    /**
     * Stub.
     *
     * @type {string}
     */
    this.dbProperty = dbProperty;

    // TODO(mats.blomdahl@gmail.com): Complete docs stub.
    /**
     * Stub.
     *
     * @type {string}
     */
    this.userProperty = userProperty;

    // TODO(mats.blomdahl@gmail.com): Complete docs stub.
    /**
     * Stub.
     *
     * @type {ScriptDb.ENUM}
     */
    this.sortStrategy = opt_sortStrategy || mstat.datasource.scriptDb.LEXICAL;

    // TODO(mats.blomdahl@gmail.com): Complete docs stub.
    /**
     * Stub.
     *
     * @type {function(?):<string,number>|Function}
     */
    this.toBaseValue = opt_toBaseValue || mstat.identityFn;

    // TODO(mats.blomdahl@gmail.com): Complete docs stub.
    /**
     * Stub.
     *
     * @type {function(?):<string,number>|Function}
     */
    this.toUserValue = opt_toUserValue || mstat.identityFn;

    // TODO(mats.blomdahl@gmail.com): Complete docs stub.
    /**
     * Stub.
     *
     * @type {function()|Function}
     */
    this.autoPopulateBaseValue = opt_autoPopulateBaseValue || function() {};

};

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Stub.
 *
 * @type {ScriptDbMemoizer_}
 * @private
 * @const
 */
var scriptDbTypeMemoizer_ = new ScriptDbMemoizer_('t', 'type', mstat.datasource.scriptDb.LEXICAL,
    // TODO(mats.blomdahl@gmail.com): Complete docs stub.
    /**
     * Stub.
     *
     * @param {string} value
     * @returns {string}
     */
    function(value) {
        return TYPE_MEMOIZER_MAPPING_.toBaseValue[value];
    },
    // TODO(mats.blomdahl@gmail.com): Complete docs stub.
    /**
     * Stub.
     *
     * @param {string} value
     * @returns {string}
     */
    function(value) {
        return TYPE_MEMOIZER_MAPPING_.toUserValue[value];
    });

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Stub.
 *
 * @type {ScriptDbMemoizer_}
 * @private
 * @const
 */
var scriptDbThreadIdMemoizer_ = new ScriptDbMemoizer_('tid', 'thread_id');

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Stub.
 *
 * @type {ScriptDbMemoizer_}
 * @private
 * @const
 */
var scriptDbOriginThreadIdMemoizer_ = new ScriptDbMemoizer_('otid', 'origin_thread_id');

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Stub.
 *
 * @type {ScriptDbMemoizer_}
 * @private
 * @const
 */
var scriptDbMessageIdMemoizer_ = new ScriptDbMemoizer_('mid', 'message_id');

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Stub.
 *
 * @type {ScriptDbMemoizer_}
 * @private
 * @const
 */
var scriptDbOriginMessageIdMemoizer_ = new ScriptDbMemoizer_('omid', 'origin_message_id');

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Stub.
 *
 * @type {ScriptDbMemoizer_}
 * @private
 * @const
 */
var scriptDbOriginDateMemoizer_ = new ScriptDbMemoizer_('odt', 'origin_date', mstat.datasource.scriptDb.LEXICAL);

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Stub.
 *
 * @type {ScriptDbMemoizer_}
 * @private
 * @const
 */
var scriptDbMessageCountMemoizer_ = new ScriptDbMemoizer_('cnt', 'message_count', mstat.datasource.scriptDb.NUMERIC);

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Stub.
 *
 * @type {ScriptDbMemoizer_}
 * @private
 * @const
 */
var scriptDbRemoveLabelMemoizer_ = new ScriptDbMemoizer_('rl', 'remove_label', mstat.datasource.scriptDb.LEXICAL,
    // TODO(mats.blomdahl@gmail.com): Complete docs stub.
    /**
     * Stub.
     *
     * @param {string} value
     * @returns {string}
     */
    function(value) {
        return LABEL_MEMOIZER_MAPPING_.removeLabel.toBaseValue[value];
    },
    // TODO(mats.blomdahl@gmail.com): Complete docs stub.
    /**
     * Stub.
     *
     * @param {string} value
     * @returns {string}
     */
    function(value) {
        return LABEL_MEMOIZER_MAPPING_.removeLabel.toUserValue[value];
    });

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Stub.
 *
 * @type {ScriptDbMemoizer_}
 * @private
 * @const
 */
var scriptDbAddLabelMemoizer_ = new ScriptDbMemoizer_('al', 'add_label', mstat.datasource.scriptDb.LEXICAL,
    // TODO(mats.blomdahl@gmail.com): Complete docs stub.
    /**
     * Stub.
     *
     * @param {string} value
     * @returns {string}
     */
    function(value) {
        return LABEL_MEMOIZER_MAPPING_.addLabel.toBaseValue[value];
    },
    // TODO(mats.blomdahl@gmail.com): Complete docs stub.
    /**
     * Stub.
     *
     * @param {string} value
     * @returns {string}
     */
    function(value) {
        return LABEL_MEMOIZER_MAPPING_.addLabel.toUserValue[value];
    });

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Stub.
 *
 * @type {ScriptDbMemoizer_}
 * @private
 * @const
 */
var scriptDbForwardToMemoizer_ = new ScriptDbMemoizer_('ft', 'forward_to', mstat.datasource.scriptDb.LEXICAL,
    // TODO(mats.blomdahl@gmail.com): Complete docs stub.
    /**
     * Stub.
     *
     * @param {string} value
     * @returns {string}
     */
    function(value) {
        return FORWARD_ADDR_MEMOIZER_MAPPING_.forwardTo.toBaseValue[value];
    },
    // TODO(mats.blomdahl@gmail.com): Complete docs stub.
    /**
     * Stub.
     *
     * @param {string} value
     * @returns {string}
     */
    function(value) {
        return FORWARD_ADDR_MEMOIZER_MAPPING_.forwardTo.toUserValue[value];
    });

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Stub.
 *
 * @type {ScriptDbMemoizer_}
 * @private
 * @const
 */
var scriptDbForwardFromMemoizer_ = new ScriptDbMemoizer_('ff', 'forward_from', mstat.datasource.scriptDb.LEXICAL,
    // TODO(mats.blomdahl@gmail.com): Complete docs stub.
    /**
     * Stub.
     *
     * @param {string} value
     * @returns {string}
     */
    function(value) {
        return FORWARD_ADDR_MEMOIZER_MAPPING_.forwardFrom.toBaseValue[value];
    },
    // TODO(mats.blomdahl@gmail.com): Complete docs stub.
    /**
     * Stub.
     *
     * @param {string} value
     * @returns {string}
     */
    function(value) {
        return FORWARD_ADDR_MEMOIZER_MAPPING_.forwardFrom.toUserValue[value];
    });

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Stub.
 *
 * @type {ScriptDbMemoizer_}
 * @private
 * @const
 */
var scriptDbSysDirtyMemoizer_ = new ScriptDbMemoizer_('df', 'sys_dirty', mstat.datasource.scriptDb.LEXICAL);

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Stub.
 *
 * @type {ScriptDbMemoizer_}
 * @private
 * @const
 */
var scriptDbSysCreatedMemoizer_ = new ScriptDbMemoizer_('cdt', 'sys_created', mstat.datasource.scriptDb.LEXICAL,
    null,
    null,
    // TODO(mats.blomdahl@gmail.com): Complete docs stub.
    /**
     * Stub.
     *
     * @param {string} value
     * @returns {string}
     */
    function() {
        return mstat.util.toIsoFormat(new Date());
    });

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Stub.
 *
 * @type {ScriptDbMemoizer_}
 * @private
 * @const
 */
var scriptDbSysModifiedMemoizer_ = new ScriptDbMemoizer_('mdt', 'sys_modified', mstat.datasource.scriptDb.LEXICAL,
    // TODO(mats.blomdahl@gmail.com): Complete docs stub.
    /**
     * Stub.
     *
     * @param {string} value
     * @returns {string}
     */
    function(value) {
        var lastModified = new Date(value);
        var now = new Date();
        var timeDx = Math.abs(lastModified.valueOf() - now.valueOf()) / 1000;
        if (timeDx > (6 * 60)) {  // i.e. ≥6 minutes
            return mstat.util.toIsoFormat(now);
        } else {
            return value;
        }
    },
    null,
    // TODO(mats.blomdahl@gmail.com): Complete docs stub.
    /**
     * Stub.
     *
     * @param {string} value
     * @returns {string}
     */
    function() {
        return mstat.util.toIsoFormat(new Date());
    });

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Stub.
 *
 * @constructor
 * @private
 */
var ScriptDbSerializer_ = function() {

    this.populateMemoizerMappings_();
};

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Stub.
 *
 * @type {Array.<ScriptDbMemoizer_>}
 * @private
 * @const
 */
ScriptDbSerializer_.prototype.registeredMemoizers_ = [
    scriptDbTypeMemoizer_,
    scriptDbThreadIdMemoizer_,
    scriptDbOriginThreadIdMemoizer_,
    scriptDbOriginDateMemoizer_,
    scriptDbMessageIdMemoizer_,
    scriptDbMessageCountMemoizer_,
    scriptDbOriginMessageIdMemoizer_,
    scriptDbForwardFromMemoizer_,
    scriptDbForwardToMemoizer_,
    scriptDbAddLabelMemoizer_,
    scriptDbRemoveLabelMemoizer_,
    scriptDbSysDirtyMemoizer_,
    scriptDbSysModifiedMemoizer_,
    scriptDbSysCreatedMemoizer_
];

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Stub.
 *
 * @type {Array.<ScriptDbMemoizer_>}
 * @private
 */
ScriptDbSerializer_.prototype.requireAllMemoizers_ = [
    scriptDbTypeMemoizer_,
    scriptDbSysDirtyMemoizer_,
    scriptDbSysModifiedMemoizer_,
    scriptDbSysCreatedMemoizer_
];

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Stub.
 *
 * @type {Array.<Array.<ScriptDbMemoizer_>>}
 * @private
 */
ScriptDbSerializer_.prototype.requireAnyMemoizers_ = [
    [scriptDbTypeMemoizer_, scriptDbThreadIdMemoizer_, scriptDbMessageCountMemoizer_, scriptDbOriginDateMemoizer_],
    [scriptDbTypeMemoizer_, scriptDbMessageIdMemoizer_, scriptDbOriginDateMemoizer_]
];

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Stub.
 *
 * @type {!Object.<string,ScriptDbMemoizer_>}
 * @private
 */
ScriptDbSerializer_.prototype.propertyToMemoizerMappings_;

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Stub.
 *
 * @private
 */
ScriptDbSerializer_.prototype.populateMemoizerMappings_ = function() {

    var memoizers = this.registeredMemoizers_;
    var mappings = {
        userProperties: {},
        dbProperties: {}
    };

    for (var i = memoizers.length; i--;) {
        mappings.userProperties[memoizers[i].userProperty] = memoizers[i];
        mappings.dbProperties[memoizers[i].dbProperty] = memoizers[i];
    }

    this.propertyToMemoizerMappings_ = mappings;
};

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Stub.
 *
 * @param {Object} userObject
 * @param {boolean=} opt_suppressAutoPopulate
 * @param {boolean=} opt_laxValidation
 * @returns {Object}
 * @throws {Error} When {@code userObject} properties are malformed/undefined.
 */
ScriptDbSerializer_.prototype.serialize = function(userObject, opt_suppressAutoPopulate, opt_laxValidation) {

    opt_suppressAutoPopulate = opt_suppressAutoPopulate || false;
    opt_laxValidation = opt_laxValidation || false;

    /* DEBUG */
    if (mstat.DEBUG) {
        Logger.log('ScriptDbSerializer_.prototype.serialize: ' + Utilities.jsonStringify(userObject));
    }
    /* DEBUG */

    var errPrefix = 'ScriptDbSerializer_.prototype.serialize:';
    var userProperties = this.propertyToMemoizerMappings_.userProperties;
    var baseObject = {};

    for (var refProperty in userProperties) {
        if (userProperties.hasOwnProperty(refProperty)) {
            var memoizer = userProperties[refProperty];
            if (userObject.hasOwnProperty(refProperty)) {
                baseObject[memoizer.dbProperty] = memoizer.toBaseValue(userObject[refProperty]);
            } else {
                if (!opt_suppressAutoPopulate) {
                    baseObject[memoizer.dbProperty] = memoizer.autoPopulateBaseValue();
                }
            }
        } else {
            throw Error(errPrefix + 'Unrecognized \'refProperty\' (\"' + refProperty + '\").');
        }
    }

    for (var property in userObject) {
        if (!userObject.hasOwnProperty(property) || !userProperties.hasOwnProperty(property)) {
            throw Error(errPrefix + 'Unrecognized property \'' + property + '\'.');
        }
    }

    if (!opt_laxValidation) {
        // Assertions stub.
        var i;
        var j;

        // {@code requireAllMemoizers_} check
        var reqAllMemoizers = this.requireAllMemoizers_;
        for (i = reqAllMemoizers.length; i--;) {
            var reqProperty = reqAllMemoizers[i].dbProperty;
            if (!baseObject.hasOwnProperty(reqProperty) || baseObject[reqProperty] === undefined) {
                throw Error(errPrefix + 'Required property \'' + reqProperty + '\' undefined.');
            }
        }

        // {@code requireAnyMemoizers_} check
        var reqAnyMemoizers = this.requireAnyMemoizers_;
        var reqAnySatisfied = false;
        for (i = reqAnyMemoizers.length; i--;) {
            var seqMismatch = false;
            for (j = reqAnyMemoizers[i].length; j--;) {
                var reqProperty = reqAnyMemoizers[i][j];
                if (!baseObject.hasOwnProperty(reqProperty)) {
                    seqMismatch = true;
                    break;
                }
            }
            if (!seqMismatch) {
                reqAnySatisfied = true;
                break;
            }
        }
        if (!reqAnySatisfied) {
            throw Error(errPrefix + '\'requireAnyMemoizers_\' validation failed.');
        }
    }

    /* DEBUG */
    if (mstat.DEBUG) {
        Logger.log('ScriptDbSerializer_.prototype.serialize: ' + Utilities.jsonStringify(baseObject));
    }
    /* DEBUG */

    return baseObject;
};

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Stub.
 *
 * @param {Object} baseObject
 * @param {boolean=} opt_suppressAutoPopulate
 * @param {boolean=} opt_laxValidation
 * @returns {Object}
 * @throws {Error} When {@code baseObject} properties are unrecognized.
 */
ScriptDbSerializer_.prototype.deserialize = function(baseObject, opt_suppressAutoPopulate, opt_laxValidation) {

    opt_suppressAutoPopulate = opt_suppressAutoPopulate || false;
    opt_laxValidation = opt_laxValidation || false;

    /* DEBUG */
    if (mstat.DEBUG) {
        Logger.log('ScriptDbSerializer_.prototype.deserialize: ' + Utilities.jsonStringify(baseObject));
    }
    /* DEBUG */

    var errPrefix = 'ScriptDbSerializer_.prototype.deserialize:';
    var dbProperties = this.propertyToMemoizerMappings_.dbProperties;
    var userObject = {};

    for (var property in baseObject) {
        if (baseObject.hasOwnProperty(property) && dbProperties.hasOwnProperty(property)) {
            var memoizer = dbProperties[property];
            userObject[memoizer.userProperty] = memoizer.toUserValue(baseObject[property]);
        } else {
            throw Error(errPrefix + 'Unrecognized property \'' + property + '\'.');
        }
    }

    /* DEBUG */
    if (mstat.DEBUG) {
        Logger.log('ScriptDbSerializer_.prototype.deserialize: ' + Utilities.jsonStringify(userObject));
    }
    /* DEBUG */

    return userObject;
};

/**
 * Implementation of the {@code mstat.datasource} namespace.
 *
 * @type {Object}
 * @const
 * @private
 */
var datasource_ = {};

// /**
//  * Stub.
//  *
//  * @constructor
//  */
// datasource_.ScriptDbRecord = function() {
//
// };

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Implementation of the {@code mstat.datasource.ScriptDb} interface.
 *
 * @param {string} dbNamespace
 * @constructor
 */
datasource_.ScriptDb = function(dbNamespace) {

    /**
     * Stub.
     *
     * @type {string}
     * @const
     * @private
     */
    this.namespace_ = dbNamespace;

    this.serializer_ = new ScriptDbSerializer_();
};

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Stub.
 *
 * @type {ScriptDbSerializer_}
 * @private
 */
datasource_.ScriptDb.prototype.serializer_;

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Stub.
 *
 * @type {string}
 * @private
 * @const
 */
datasource_.ScriptDb.prototype.threadType_ = 'thread';

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Stub.
 *
 * @type {string}
 * @private
 * @const
 */
datasource_.ScriptDb.prototype.threadRmType_ = 'thread-rm';

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Stub.
 *
 * @type {string}
 * @private
 * @const
 */
datasource_.ScriptDb.prototype.messageType_ = 'message';

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Stub.
 *
 * @type {string}
 * @private
 * @const
 */
datasource_.ScriptDb.prototype.messageRmType_ = 'message-rm';

/**
 * Adds new record to the ScriptDb database (no validation).
 *
 * @param {Object} baseObject
 * @returns {Object} ScriptDb record.
 * @private
 */
datasource_.ScriptDb.prototype.add_ = function(baseObject) {

    /** @typedef {datasource_.ScriptProp} */
    mstat.datasource.ScriptProp.updateScriptDbSize(baseObject);
    mstat.datasource.ScriptProp.updateScriptDbCount(1);

    mstat.datasource.scriptDb.save(baseObject);
};

/**
 * Removes old ScriptDb record, adds replacement record (no validation).
 *
 * @param {Object} addBaseObject
 * @param {Object} removeBaseObject
 * @returns {Object} ScriptDb record.
 * @private
 */
datasource_.ScriptDb.prototype.replace_ = function(addBaseObject, removeBaseObject) {

    /** @typedef {datasource_.ScriptProp} */
    mstat.datasource.ScriptProp.updateScriptDbSize(addBaseObject, removeBaseObject);

    mstat.datasource.scriptDb.remove(removeBaseObject);
    return mstat.datasource.scriptDb.save(addBaseObject);
};

/**
 * Removes ScriptDb record (no validation).
 *
 * @param {Object} baseObject
 * @private
 */
datasource_.ScriptDb.prototype.remove_ = function(baseObject) {

    /** @typedef {datasource_.ScriptProp} */
    mstat.datasource.ScriptProp.updateScriptDbSize(null, baseObject);
    mstat.datasource.ScriptProp.updateScriptDbCount(-1);

    mstat.datasource.scriptDb.remove(baseObject);
};

// TODO(mats.blomdahl@gmail.com): Restructure poor docs.
/**
 * Queries ScriptDb using the 'query by example' pattern.
 *
 * Supported query options ({@code opt_qOpts} parameter):
 * <ul>
 *     <li><pre>limit</pre></li>
 *     <li><pre>startAt</pre></li>
 *     <li><pre>sortBy</pre></li>
 *     <li><pre>sortDirection</pre></li>
 * </ul>
 *
 * @param {Object} queryCfg The query configuration.
 * @param {!Object=} opt_qOpts ScriptDb query options.
 * @returns {Array.<Object>} Retrieved ScriptDb records.
 * @private
 */
datasource_.ScriptDb.prototype.query_ = function(queryCfg, opt_qOpts) {
    if (!queryCfg) {
        throw Error('Required \'queryCfg\' parameter missing.');
    }

    opt_qOpts = opt_qOpts || {
        startAt: 0,
        limit: 1000,
        sortBy: "sys_created"
    };

    var results = mstat.datasource.scriptDb.query(this.serializer.serialize(queryCfg, true, true));

    if (opt_qOpts.startAt) {
        results = results.startAt(opt_qOpts.startAt);
    }

    if (opt_qOpts.limit) {
        results = results.limit(opt_qOpts.limit);
    }

    if (opt_qOpts.sortBy) {
        var dbProperty = _toScriptDbTrans[opt_qOpts.sortBy].dbProperty;
        var sortDirection = opt_qOpts.sortDirection || mstat.datasource.scriptDb.ASCENDING;
        var sortStrategy = _toScriptDbTrans[opt_qOpts.sortBy].sortStrategy;
        results = results.sortBy(dbProperty, sortDirection, sortStrategy);
    }

    /* DEBUG */
    if (mstat.DEBUG) {
        Logger.log('datasource_.ScriptDb.prototype.query_: results.getSize()=' + results.getSize() +
            ' (opt_qOpts: \"' + Utilities.jsonStringify(opt_qOpts) + '\")');
    }
    /* DEBUG */

    var items = [];
    while (results.hasNext()) {
        var rawItem = results.next();
        items.push({
            item: this.serializer_.deserialize(rawItem),
            rawItem: rawItem
        });
    }

    return items;
};

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Adds a {@code GmailThread} repr. record to ScriptDb (duplicate check enabled).
 *
 * @param {Object} thread
 * @param {boolean=} opt_strictDuplicateCheck Strict duplicate check, defaults to false.
 * @returns {Object|false}
 * @throws {Error} When attempting to write duplicate entries with {@code opt_strictDuplicateCheck} enabled.
 */
datasource_.ScriptDb.prototype.addThread = function(thread, opt_strictDuplicateCheck) {

    opt_strictDuplicateCheck = opt_strictDuplicateCheck || false;

    var items = this.query_({
        type: this.threadType_,
        thread_id: thread.thread_id
    });

    if (items.length) {
        if (opt_strictDuplicateCheck) {
            throw Error('Duplicate \'thread_id\' (\"' + thread.thread_id + '\").');
        } else {
            return false;
        }
    } else {
        var now = new Date();
        thread.type = this.threadType_;
        thread.sys_modified = thread.sys_created = mstat.util.toIsoFormat(now);
        var baseObject = this.serializer_.serialize(thread, true, true);
        return {
            item: thread,
            rawItem: this.add_(baseObject)
        };
    }
};

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Adds a {@code GmailMessage} repr. record to ScriptDb (duplicate check enabled).
 *
 * @param {Object} message
 * @param {boolean=} opt_strictDuplicateCheck Strict duplicate check, defaults to false.
 * @returns {Object|false}
 * @throws {Error} When attempting to write duplicate entries with {@code opt_strictDuplicateCheck} enabled.
 */
datasource_.ScriptDb.prototype.addMessage = function(message, opt_strictDuplicateCheck) {

    opt_strictDuplicateCheck = opt_strictDuplicateCheck || false;

    var items = this.query_({
        type: this.messageType_,
        message_id: message.message_id
    });

    if (items.length) {
        if (opt_strictDuplicateCheck) {
            throw Error('Duplicate \'message_id\' (\"' + message.message_id + '\").');
        } else {
            return false;
        }
    } else {
        var now = new Date();
        message.type = this.messageType_;
        message.sys_modified = message.sys_created = mstat.util.toIsoFormat(now);
        var baseObject = this.serializer_.serialize(message, true, true);
        return {
            item: message,
            rawItem: this.add_(baseObject)
        };
    }
};

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Adds a removed {@code GmailThread} repr. record to ScriptDb (duplicate check enabled).
 *
 * @param {Object} thread
 * @param {boolean=} opt_strictDuplicateCheck Strict duplicate check, defaults to false.
 * @returns {Object|false}
 * @throws {Error} When attempting to write duplicate entries with {@code opt_strictDuplicateCheck} enabled.
 */
datasource_.ScriptDb.prototype.addRmThread = function(thread, opt_strictDuplicateCheck) {

    opt_strictDuplicateCheck = opt_strictDuplicateCheck || false;

    var items = this.query_({
        type: this.threadRmType_,
        thread_id: thread.thread_id
    });

    if (items.length) {
        if (opt_strictDuplicateCheck) {
            throw Error('Duplicate \'thread_id\' (\"' + thread.thread_id + '\").');
        } else {
            return false;
        }
    } else {
        var now = new Date();
        thread.type = this.threadRmType_;
        thread.sys_modified = thread.sys_created = mstat.util.toIsoFormat(now);
        var baseObject = this.serializer_.serialize(thread, true, true);
        return {
            item: thread,
            rawItem: this.add_(baseObject)
        };
    }
};

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Adds a removed {@code GmailMessage} repr. record to ScriptDb (duplicate check enabled).
 *
 * @param {Object} message
 * @param {boolean=} opt_strictDuplicateCheck Strict duplicate check, defaults to false.
 * @returns {Object|false}
 * @throws {Error} When attempting to write duplicate entries with {@code opt_strictDuplicateCheck} enabled.
 */
datasource_.ScriptDb.prototype.addRmMessage = function(message, opt_strictDuplicateCheck) {

    opt_strictDuplicateCheck = opt_strictDuplicateCheck || false;

    var items = this.query_({
        type: this.messageRmType_,
        message_id: message.message_id
    });

    if (items.length) {
        if (opt_strictDuplicateCheck) {
            throw Error('Duplicate \'message_id\' (\"' + message.message_id + '\").');
        } else {
            return false;
        }
    } else {
        var now = new Date();
        message.type = this.messageRmType_;
        message.sys_modified = message.sys_created = mstat.util.toIsoFormat(now);
        var baseObject = this.serializer_.serialize(message, true, true);
        return {
            item: message,
            rawItem: this.add_(baseObject)
        };
    }
};

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Replaces a {@code GmailThread} repr. record in ScriptDb (one-to-one check enabled).
 *
 * @param {Object} thread
 * @param {boolean=} opt_strictDuplicateCheck Strict duplicate check, defaults to false.
 * @returns {Object}|false}
 * @throws {Error} When ScriptDb query fails to identify exactly 1 replacement target with
 *                 {@code opt_strictDuplicateCheck} enabled.
 */
datasource_.ScriptDb.prototype.replaceThread = function(thread, opt_strictDuplicateCheck) {

    opt_strictDuplicateCheck = opt_strictDuplicateCheck || false;

    var items = this.query_({
        type: this.threadType_,
        thread_id: thread.thread_id
    });

    if (items.length) {
        if (items.length !== 1) {
            if (opt_strictDuplicateCheck) {
                throw Error('Multiple target records (' + items.length + ') for \'thread_id\' \"' + thread.thread_id +
                    '\" (items: \"' + Utilities.jsonStringify(items) + '\")');
            } else {
                this.remove_(items[0].rawItem);
                return false;
            }
        } else {
            var oldItem = items[0].item;
            thread.sys_modified = mstat.util.toIsoFormat(new Date());
            thread = mstat.applyIf(oldItem, thread);
            var baseObject = this.serializer_.serialize(thread, true, true);
            return {
                item: thread,
                rawItem: this.replace_(baseObject, items[0].rawItem)
            };
        }
    } else {
        if (opt_strictDuplicateCheck) {
            throw Error('Target records unidentified for \'thread_id\' replacement op. (\"' + thread.thread_id +
                '\").');
        } else {
            return false;
        }
    }
};

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Replaces a {@code GmailMessage} repr. record in ScriptDb (one-to-one check enabled).
 *
 * @param {Object} message
 * @param {boolean=} opt_strictDuplicateCheck Strict duplicate check, defaults to false.
 * @returns {Object}|false}
 * @throws {Error} When ScriptDb query fails to identify exactly 1 replacement target with
 *                 {@code opt_strictDuplicateCheck} enabled.
 */
datasource_.ScriptDb.prototype.replaceMessage = function(message, opt_strictDuplicateCheck) {

    opt_strictDuplicateCheck = opt_strictDuplicateCheck || false;

    var items = this.query_({
        type: this.threadType_,
        message_id: message.message_id
    });

    if (items.length) {
        if (items.length !== 1) {
            if (opt_strictDuplicateCheck) {
                throw Error('Multiple target records (' + items.length + ') for \'message_id\' \"' +
                    message.message_id + '\" (items: \"' + Utilities.jsonStringify(items) + '\")');
            } else {
                this.remove_(items[0].rawItem);
                return false;
            }
        } else {
            var oldItem = items[0].item;
            message.sys_modified = mstat.util.toIsoFormat(new Date());
            message = mstat.applyIf(oldItem, message);
            var baseObject = this.serializer_.serialize(message, true, true);
            return {
                item: message,
                rawItem: this.replace_(baseObject, items[0].rawItem)
            };
        }
    } else {
        if (opt_strictDuplicateCheck) {
            throw Error('Target records unidentified for \'message_id\' replacement op. (\"' + message.message_id +
                '\").');
        } else {
            return false;
        }
    }
};

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Queries ScriptDb for {@code GmailThread} repr. records.
 *
 * @param {Object} queryCfg The query configuration.
 * @param {!Object=} opt_qOpts ScriptDb query options.
 * @returns {Array.<Object>} The query results.
 */
datasource_.ScriptDb.prototype.getThreads = function(queryCfg, opt_qOpts) {

    opt_qOpts = opt_qOpts || null;

    queryCfg.type = this.threadType_;

    return this.query_(queryCfg, opt_qOpts);
};

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Queries ScriptDb for {@code GmailMessage} repr. records.
 *
 * @param {Object} queryCfg The query configuration.
 * @param {!Object=} opt_qOpts ScriptDb query options.
 * @returns {Array.<Object>} The query results.
 */
datasource_.ScriptDb.prototype.getMessages = function(queryCfg, opt_qOpts) {

    opt_qOpts = opt_qOpts || null;

    queryCfg.type = this.messageType_;

    return this.query_(queryCfg, opt_qOpts);
};

// TODO(mats.blomdahl@gmail.com): Complete docs stub.
/**
 * Deletes all ScriptDb records (disabled by default; it's irreversible and extremely destructive).
 *
 * @param {boolean} yes
 * @returns {number}
 * @private
 */
datasource_.ScriptDb.prototype.flushScriptDb_ = function(yes) {
    if (false && !yes) {
        throw Error('Plz not do this lightly');
    }
    var deletedCount = 0;
    while (true) {
        var result = db.query({}); // Get everything, up to limit (defaults to 200).
        if (result.getSize() == 0) {
            break;
        }
        while (result.hasNext()) {
            deletedCount++;
            db.remove(result.next());
        }
        Logger.log(toLogLine('Deleted ' + deletedCount + ' items...'));
    }
    return deletedCount;
};

/**
 * Implementation of the {@code mstat.datasource.ScriptProp} interface.
 *
 * @param {string} propertyNamespace
 * @param {function(string, Object, number, number, number)=} opt_dailyStatsResetCallback
 * @constructor
 */
datasource_.ScriptProp = function(propertyNamespace, opt_dailyStatsResetCallback) {

    /**
     * Stub.
     *
     * @type {string}
     * @const
     * @private
     */
    this.namespace_ = propertyNamespace;

    /**
     * Total item count for ScriptDb utilization.
     *
     * @type {number}
     * @private
     */
    this.scriptDbItemCount_ = parseInt(ScriptProperties.getProperty(propertyNamespace + 'ScriptDbCount')) || 0;

    /**
     * Total items size for ScriptDb storage.
     *
     * @type {number}
     * @private
     */
    this.scriptDbItemsSize_ = parseInt(ScriptProperties.getProperty(propertyNamespace + 'ScriptDbSize')) || 0;

    /**
     * Mapping of daily forwarding counts by forwarding address.
     *
     * @type {Object}
     * @private
     */
    this.forwardedDailyStats_ = (function() {
        var dailyStats = ScriptProperties.getProperty(propertyNamespace + 'ForwardedDailyStats');
        if (dailyStats) {
            return Utilities.jsonParse(dailyStats);
        } else {
            return {};
        }
    })();

    /**
     * Stub.
     *
     * @type {number}
     * @private
     */
    this.scriptDbIngestionDailyStats_ = parseInt(ScriptProperties.getProperty('ScriptDbIngestionDailyStats')) || 0;

    /**
     * Stub.
     *
     * @type {number}
     * @private
     */
    this.lastDailyStatsBreakpoint_ = parseInt(ScriptProperties.getProperty(propertyNamespace +
        'LastDailyStatsBreakpoint')) || new Date().valueOf();

    /**
     * Stub.
     *
     * @type {number}
     * @private
     */
    this.propertyPushTimestamp_ = new Date().valueOf();

    if (opt_dailyStatsResetCallback) {
        this.dailyStatsResetCallback = opt_dailyStatsResetCallback;
    }

};

/**
 * Stub.
 *
 * @param {!Object=} opt_addedItem
 * @param {!Object=} opt_removedItem
 * @returns {number} Size delta (bytes)
 */
datasource_.ScriptProp.prototype.updateScriptDbSize = function(opt_addedItem, opt_removedItem) {
    var sizeDx = 0;
    if (opt_addedItem) {
        sizeDx += Utilities.jsonStringify(opt_addedItem).length;
    }
    if (opt_removedItem) {
        sizeDx -= Utilities.jsonStringify(opt_removedItem).length;
    }

    this.scriptDbItemsSize_ += sizeDx;

    /* DEBUG */
    if (mstat.DEBUG) {
        Logger.log(mstat.util.toLogLine('datasource_.ScriptProp._scriptDbItemSize: ' + this.scriptDbItemsSize_));
    }
    /* DEBUG */

    return sizeDx;
};

/**
 * Stub.
 *
 * @param {number} countDx
 * @returns {number} Count delta
 */
datasource_.ScriptProp.prototype.updateScriptDbCount = function(countDx) {
    if (countDx) {
        this.scriptDbItemCount_ += countDx;
    }

    /* DEBUG */
    if (mstat.DEBUG) {
        Logger.log(mstat.util.toLogLine('datasource_.ScriptProp._scriptDbItemCount: ' + this.scriptDbItemCount_));
    }
    /* DEBUG */

    return countDx;
};

/**
 * Stub.
 *
 * @param {number} countDx
 * @param {boolean=} opt_flush
 * @returns {number}
 */
datasource_.ScriptProp.prototype.updateScriptDbIngestionsDailyStats = function(countDx, opt_flush) {
    if (countDx) {
        this.scriptDbIngestionDailyStats_ += countDx;
    }

    if (opt_flush) {
        var oldValue = this.scriptDbIngestionDailyStats_;
        this.scriptDbIngestionDailyStats_ = 0;
        return oldValue;
    }

    /* DEBUG */
    if (mstat.DEBUG) {
        Logger.log(mstat.util.toLogLine('datasource_.ScriptProp._scriptDbIngestionDailyStats: ' +
            this.scriptDbIngestionDailyStats_));
    }
    /* DEBUG */

    return countDx;
};

/**
 * Stub.
 *
 * @param {Object} dxMapping
 * @param {boolean=} opt_flush
 * @returns {Object}
 */
datasource_.ScriptProp.prototype.updateForwardedDailyStats = function(dxMapping, opt_flush) {
    if (dxMapping) {
        for (var forwardTo in dxMapping) {
            if (this.forwardedDailyStats_[forwardTo] === undefined) {
                this.forwardedDailyStats_[forwardTo] = dxMapping[forwardTo];
            } else {
                this.forwardedDailyStats_[forwardTo] += dxMapping[forwardTo];
            }
        }
    }

    if (opt_flush) {
        var oldMapping = mstat.apply(this.forwardedDailyStats_);
        this.forwardedDailyStats_ = {};
        return oldMapping;
    }

    /* DEBUG */
    if (mstat.DEBUG) {
        Logger.log(mstat.util.toLogLine('datasource_.ScriptProp.forwardedDailyStats_: ' +
            Utilities.jsonStringify(this.forwardedDailyStats_)));
    }
    /* DEBUG */

    return this.forwardedDailyStats_;
};

/**
 * Stub.
 *
 * @param {string} summaryDate
 * @param {Object} forwardedMapping
 * @param {number} scriptDbIngestions
 * @param {number} scriptDbTotalCount
 * @param {number} scriptDbTotalSize
 */
datasource_.ScriptProp.prototype.dailyStatsResetCallback = function(summaryDate, forwardedMapping, scriptDbIngestions,
                                                         scriptDbTotalCount, scriptDbTotalSize) {
    var msg = Utilities.formatString('Daily Forwarding Stats Calculated for %s\n\n', summaryDate);
    var tpl = '    %34s:\t%3i\n';
    for (var forwardedTo in forwardedMapping) {
        msg += Utilities.formatString(tpl, forwardedTo, forwardedMapping[forwardedTo]);
    }
    msg += Utilities.formatString('\n\n    %34s:\t%9i\n\n', 'Today\'s ScriptDb Ingestions', scriptDbIngestions);
    msg += Utilities.formatString('\n\n    %34s:\t%9i\n', 'ScriptDb Total Count', scriptDbTotalCount);

    if (scriptDbTotalSize > 1024 * 1024 * 10) {
        // Display MB
        scriptDbTotalSize = Utilities.formatString('%3.2f MB', scriptDbTotalSize / (1024 * 1024));

    } else {
        // Display kB
        scriptDbTotalSize = Utilities.formatString('%i kB', scriptDbTotalSize / 1024);
    }

    msg += Utilities.formatString('\n\n    %34s:\t%9s\n\n', 'ScriptDb Total Size', scriptDbTotalSize);
    var logLine = mstat.util.toLogLine(msg);

    /* DEBUG */
    if (mstat.DEBUG) {
        Logger.log(logLine);
    }
    /* DEBUG */

    GmailApp.sendEmail('mats.blomdahl@gmail.com', 'Email Forwarder: Heartbeat', logLine);
};

/**
 * Stub.
 *
 * @param {boolean=} opt_wait
 * @returns {boolean}
 */
datasource_.ScriptProp.prototype.pushPropertyState = function(opt_wait) {
    opt_wait = opt_wait || false;
    var now = new Date().valueOf();
    if ((now - this.propertyPushTimestamp_) < 1000) {
        if (!opt_wait) {
            return false;
        } else {
            Utilities.sleep(1000 - (now - this.propertyPushTimestamp_));
            now = new Date().valueOf();
        }
    }
    this.propertyPushTimestamp_ = now;

    var propertiesUpdate = {
        'emailProcessorScriptDbCount': String(this.scriptDbItemCount_),
        'emailProcessorScriptDbSize': String(this.scriptDbItemsSize_)
    };

    if ((now - this.lastDailyStatsBreakpoint_) > 3600 * 24 * 1000) {
        // Flush daily stats and summarize
        var forwarded = this.updateForwardedDailyStats(null, true);
        var scriptDbIngested = this.updateScriptDbIngestionsDailyStats(null, true);
        var summaryDate = Utilities.formatDate(new Date(now - 3600 * 24 * 1000), "UT", "yyyy-MM-dd");
        var scriptDbTotalCount = this.scriptDbItemCount_;
        var scriptDbTotalSize = this.scriptDbItemsSize_;

        this.lastDailyStatsBreakpoint_ = now;

        propertiesUpdate[Utilities.formatString('%sDailyStats_%s', this.namespace_, summaryDate)] =
            Utilities.jsonStringify({
                date: summaryDate,
                forwarded: forwarded,
                script_db_ingestions: scriptDbIngested,
                script_db_total_count: scriptDbTotalCount,
                script_db_total_size: scriptDbTotalSize
            });

        this.dailyStatsResetCallback(summaryDate, forwarded, scriptDbIngested, scriptDbTotalCount, scriptDbTotalSize);
    }

    var dailyStats = {};
    dailyStats[Utilities.formatString('%sForwardedDailyStats', this.namespace_)] =
        Utilities.jsonStringify(this.forwardedDailyStats_);
    dailyStats[Utilities.formatString('%sScriptDbIngestionDailyStats', this.namespace_)] =
        String(this.scriptDbIngestionDailyStats_);
    dailyStats[Utilities.formatString('%sLastDailyStatsBreakpoint', this.namespace_)] =
        String(this.lastDailyStatsBreakpoint_);

    propertiesUpdate = mstat.applyIf(dailyStats, propertiesUpdate);

    ScriptProperties.setProperties(propertiesUpdate);

    return true;
};




